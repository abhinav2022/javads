class Solution {
    int minDist(int a[], int n, int x, int y) {
        // code here
        int p1=-1,p2=-1,l=Integer.MAX_VALUE;
        
        for(int i=0;i<a.length;i++){
            if(a[i]==x){
                p1=i;
               
                    
            }
            if(a[i]==y){
                p2=i;
            }
            
            if(p1!=-1 && p2!=-1){
                 int u= p1-p2>0?p1-p2:p2-p1;
                 if(u<l){
                     l=u;
                 }
            }
        }
        
        if(p1==-1 || p2==-1){
            return -1;
        }else{
            return l;
        }
    }
}