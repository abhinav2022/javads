class Solution {
    public int[] plusOne(int[] arr) {
        int arr1[];
        arr[arr.length-1]+=1;
       int cnt=0,p=0,flag=0;
        for(int i=arr.length-1;i>=0;i--){
            if(arr[i]>9){
                p=arr[i]/10;
                arr[i]=arr[i]%10;
                if(i-1>=0)
                    arr[i-1]+=p;
                
            }else{
                flag=1;
                break;
            }
        }
        if(flag==1){
            return arr;
        }else{
            arr1=new int[arr.length+1];
            arr1[0]=p;

            for(int i=1;i<arr1.length;i++){
                arr1[i]=arr[i-1];
            }
        }

        return arr1;
    }
}