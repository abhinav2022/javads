import java.util.*;
class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> h1 = new HashSet<>();
        HashSet<Integer> h2 = new HashSet<>();
        
        for(int i=0;i<nums1.length;i++){
            h1.add(nums1[i]);
        }
          for(int i=0;i<nums2.length;i++){
              if(h1.contains(nums2[i]))
                    h2.add(nums2[i]);
        }
        int arr2[] = new int[h2.size()];
        int i=0;
        for(int p:h2){
                arr2[i++]=p;
        }
            return arr2;
        
    }
}