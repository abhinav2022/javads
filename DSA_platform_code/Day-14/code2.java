
class Solve {
    int[] printUnsorted(int[] arr, int n) {
        // code here
        int i=0,j=n-1;
        int arr2[]=new int[2];
        while(i<n-1 && arr[i]<=arr[i+1]){
                i++;
        }
        while(j>0 && arr[j]>=arr[j-1]){
                j--;
        }
        if(i==n-1){
            arr2[0]=0;
            arr2[1]=0;
            return arr2;
        }
        
        int min1=Integer.MAX_VALUE,max1=Integer.MIN_VALUE;
        for(int y=i;y<=j;y++){
            min1=Math.min(min1,arr[y]);
            max1=Math.max(max1,arr[y]);
        }
       
       while(i>0 && arr[i-1]>min1){
           i--;
       }
       while(j<n-1 && arr[j+1]<max1){
           j++;
       }
        
        arr2[0]=i;
        arr2[1]=j;
        return arr2;
    }
}