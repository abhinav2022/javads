class Solution {

    public static int smallestSubWithSum(int arr[], int n, int x) {
        // Your code goes here 
        int sum=0,ps=0,min=Integer.MAX_VALUE;
       for(int i=0;i<n;i++){
           sum+=arr[i];
           
           while(sum>x){
               min=Math.min(min,i-ps+1);
               sum-=arr[ps++];
           }
       }
       
       if(min==Integer.MAX_VALUE)
            return 0;
       
       
        return min;
    }
}

