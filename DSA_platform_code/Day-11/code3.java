

//User function Template for Java


class Compute {
    public String isSubset( long a1[], long a2[], long n, long m) {
        HashMap<Long,Integer> hm1 = new HashMap<>();
        
        for(int i=0;i<a1.length;i++){
            long p= a1[i];
            if(hm1.containsKey(p))
                hm1.put(p,(int)hm1.get(p)+1);
            else
                hm1.put(p,1);
        }
        HashMap<Long,Integer> hm2 = new HashMap<>();
        for(int i=0;i<a2.length;i++){
            long p=a2[i];
            if(hm2.containsKey(p))
                hm2.put(p,(int)hm2.get(p)+1);
            else
                hm2.put(p,1);
        }
        
        for(int i=0;i<a2.length;i++){
            if(hm1.containsKey(a2[i]) && hm1.get(a2[i])!=0){
              
                hm1.put(a2[i],hm1.get(a2[i])-1);
            }else
                return "No";
        }
        
        return "Yes";
        
    }
}