class Solution {
    public int removeElement(int[] nums, int val) {
        int b=0;
        int arr[]=new int[nums.length];
        for(int i=0;i<nums.length;i++){
            if(nums[i]!=val){
                arr[b++]=nums[i];
            }
        }
        for(int i=0;i<arr.length;i++){
            nums[i]=arr[i];
        }
        arr=null;
        return b;
    }
}