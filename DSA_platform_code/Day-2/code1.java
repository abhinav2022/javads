class Solution {
    public int[] twoSum(int[] arr, int target) {
        int arr2[]= new int[2];

        HashMap <Integer,Integer> hm = new HashMap<Integer,Integer>();

        for(int i=0;i<arr.length;i++){
            int x= target-arr[i];
            if(hm.containsKey(x)){
                arr2[0]=(int)hm.get(x);
                arr2[1]=i;
            }else{
                hm.put(arr[i],i);
            }
        }
        return arr2;
    }
}