class Solution {
    public List<Integer> fun(int t){
        List<Integer> l = new ArrayList<Integer>();
        int ans=1;
        l.add(1);
        for(int i=1;i<t;i++){
            
            ans=ans*(t-i);
            ans/=i;
            l.add(ans);
        }
        return l;
    } 

    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>>  ll = new ArrayList<List<Integer>>();

        for(int i=1;i<=numRows;i++){
            ll.add(fun(i));
        }
        return ll;
    }
}