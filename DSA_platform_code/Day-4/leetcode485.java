class Solution {
    public int findMaxConsecutiveOnes(int[] arr) {
        int r=0,cnt=0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]==0){
                if(cnt>r)
                    r=cnt;
                    cnt=0;
            }
            if(arr[i]==1){
                cnt++;
            }
        }
        if(cnt>r)
                    r=cnt;
                   
        return r;
    }
}