import java.util.*;
class Solution {
    public int removeDuplicates(int[] arr) {
        
        Vector<Integer> v = new Vector<Integer>();
        v.add(arr[0]);
        for(int i=1;i<arr.length;i++){
            if(arr[i]!=arr[i-1]){
                v.add(arr[i]);
            }
        }
        int i=0;
        for( i=0;i<v.size();i++){
            arr[i]=v.get(i);
        }
        v=null;
        return i;
    }
}