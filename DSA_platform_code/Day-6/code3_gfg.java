class Solution {
    long maxProduct(int[] arr, int n) {
        // code here
      Arrays.sort(arr);
      long p1=(long)arr[0]*arr[1]*arr[n-1];
      long p2 = (long)arr[n-1]*arr[n-2]*arr[n-3];
      
      return p1>p2?p1:p2;
    }
}