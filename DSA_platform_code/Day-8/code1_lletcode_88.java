class Solution {
    public void merge(int[] nums1, int i, int[] nums2, int j) {
        int m=i-1;
        int n=j-1;
        int y=i+j-1;
            while(m>=0 && n>=0){
                if(nums1[m]<nums2[n]){
                    nums1[y--]=nums2[n--];
                }else {
                    nums1[y--]=nums1[m--];
                }
            }
            while(n>=0){
                nums1[y--]=nums2[n--];
            }
    }
}