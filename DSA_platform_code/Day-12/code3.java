/*
class pair  {  
    long first, second;  
    public pair(long first, long second)  
    {  
        this.first = first;  
        this.second = second;  
    }  
}
*/

class Solution {
    
    public pair indexes(long v[], long x)
    {
        pair pp=new pair(-1,-1);
        for(int i=0;i<v.length;i++){
            if(v[i]==x){
                if(pp.first==-1)
                    pp.first=i;
                
                pp.second=i;
            }
        }
        
        return pp;
    }
}