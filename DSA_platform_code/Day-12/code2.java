class Solution{
    
    // Function for finding maximum and value pair
    public static long find_multiplication (int arr[], int brr[], int n, int m) {
        // Complete the Function
        int p1=Integer.MAX_VALUE,p2=Integer.MIN_VALUE;
        for(int i=0;i<arr.length;i++){
            if(p2<arr[i])
            p2=arr[i];
        }
         for(int i=0;i<brr.length;i++){
            if(p1>brr[i])
                p1=brr[i];
        }
        
        return (long)(p1*p2);
    }
    
    
}

