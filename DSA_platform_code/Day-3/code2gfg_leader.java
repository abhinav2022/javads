class Solution {
    public int majorityElement(int[] arr) {
        int y=Integer.MIN_VALUE,m=0;
        HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();
        for(int i=0;i<arr.length;i++){
            if(hm.containsKey(arr[i])){
                hm.put(arr[i],hm.get(arr[i])+1);
            }else{
                hm.put(arr[i],1);
            }
        }

        Iterator itr =hm.entrySet().iterator();

        while(itr.hasNext()){
            Map.Entry mp =(Map.Entry) itr.next();
            int j=(int)mp.getValue();
            if(y<j){
                y=j;
                m=(int)mp.getKey();
            }
        }
        return m;
    }
}