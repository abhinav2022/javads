class Solution {
    // Function to return the position of the first repeating element.
    public static int firstRepeated(int[] arr, int n) {
        // Your code here
        HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();
         
         for(int i=0;i<arr.length;i++){
             if(hm.containsKey(arr[i])){
                 hm.put(arr[i],hm.get(arr[i])+1);
             }else{
                 hm.put(arr[i],1);
             }
         }
         
         for(int i=0;i<arr.length;i++){
             if((int)hm.get(arr[i])>1)
                return i+1;
         }
         return -1;
    }
}
