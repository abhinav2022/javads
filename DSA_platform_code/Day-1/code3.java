
class Solution {
    void fun(int arr[],int start,int mid,int end){
        int l1=mid-start+1;
        int l2=end-mid;
        
        int arr1[]=new int[l1];
        int arr2[]=new int[l2];
        
        for(int i=0;i<l1;i++){
            arr1[i]=arr[start+i];
        }
        for(int i=0;i<l2;i++){
            arr2[i]=arr[mid+i+1];
        }
            int itr1=0,itr2=0,itr3=start;
        while(itr1<l1 && itr2<l2){
            if(arr1[itr1]>=arr2[itr2]){
                arr[itr3]=arr2[itr2++];
            }else{
                arr[itr3]=arr1[itr1++];
            }
            itr3++;
        }
        while(itr1<l1){
            arr[itr3++]=arr1[itr1++];
        }
        while(itr2<l2){
            arr[itr3++]=arr2[itr2++];
        }
    }
    void merge(int arr[],int start,int end){
        if(start<end){
            int mid =(start+end)/2;
            merge(arr,start,mid);
            merge(arr,mid+1,end);
            fun(arr,start,mid,end);
        }
    }
    boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        // code here
        merge(arr,0,arr.length-1);
         //Arrays.sort(arr);
        int i=0,l=arr.length-1;
        while(i<l){
            if(arr[i]+arr[l]==x)
            return true;
            else if(arr[i]+arr[l]<x)
            i++;
            else
            l--;
        }
        
        return false;
    }
}