class Solution {
    public int singleNumber(int[] arr) {
        HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();

        for(int i=0;i<arr.length;i++){
            if(hm.containsKey(arr[i])){
                hm.put(arr[i],hm.get(arr[i])+1);
            }else
                hm.put(arr[i],1);
        }
            Iterator itr = hm.entrySet().iterator();

            while(itr.hasNext()){
                Map.Entry mp =(Map.Entry) itr.next();
                //Integer p=(Integer)mp.getValue();
                int pp=((int)mp.getValue());//.intValue();
                if(pp==1){
                   // p=mp.getKey();
                   // pp=p.intValue();
                    return (int)mp.getKey();
                }
            }
            return 0;
    }
}