class Solution {
    public int missingNumber(int[] arr) {
        Arrays.sort(arr);
        if(arr.length==1){
            if(arr[0]==0)
                return 1;
            else
                return 0;
        }
        int flag=0;
        for(int i=0;i<arr.length-1;i++){
            if(arr[i]==0)
                flag=1;
            if(arr[i+1]-arr[i]>1)
                return arr[i]+1;
        }
        if(flag==0)
            return 0;
            
        return arr.length;
    }
}