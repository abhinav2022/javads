class Solution {
    int findMaximum(int[] arr, int n) {
        // code here
        int start=0,end=n-1,mid=0,max=Integer.MIN_VALUE;

        if(arr[n-1]>arr[n-2])
            return arr[n-1];

        while(start<=end){
            mid=(start+end)/2;
            
           
            if(arr[mid]>arr[mid+1]){
                max=Math.max(max,arr[mid]);
                end=mid-1;
            }else if(arr[mid]<arr[mid+1]){
                max=Math.max(max,arr[mid+1]);
                start=mid+1;
            }
        }
        
        return max;
    }
}