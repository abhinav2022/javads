
import java.util.*;

class MergeStacks {

	Stack<Integer> mergeStacks(Stack<Integer> s1,Stack<Integer> s2){
	
		Stack<Integer> s3 = new Stack<Integer>();

		while(!s1.empty() && !s2.empty()){
		
			if(s1.peek() > s2.peek()){
			
				s3.push(s1.pop());
			}else
				s3.push(s2.pop());
		}

		while(!s1.empty()){
		
			s3.push(s1.pop());
		}

		while(!s2.empty()){
				
			s3.push(s2.pop());
		}

		return s3;
	}
}

class Client{

	public static void main(){}
}
