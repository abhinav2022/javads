
import java.util.*;

class TwoStack{

	int stackArr[];
	int top1;
	int top2;
	int maxSize;

	TwoStack(int size){
	
		this.stackArr= new int[size];
		this.top1=-1;
		this.top2=size;
		this.maxSize=size;
	}

	void push1(int data){
	
		if(top2-top1>1){
		
			stackArr[++top1]=data;
		}else
			System.out.println("Stack Overflow");
	}
	void push2(int data){
	
		if(top2-top1>1){
		
			stackArr[--top2]=data;
		}else
			System.out.println("Stack Overflow");
	}

	int pop1(){
	
		if(top1==-1){
		
			System.out.println("StackUnderflow");
			return -1;
		}else{
		
			int val=stackArr[top1--];
			return val;
		}
	}
	int pop2(){
	
		if(top2==maxSize){
		
			System.out.println("StackUnderflow");
			return -1;
		}else{
		
			int val=stackArr[top2++];
			return val;
		}
	}
}

class Client{

	public static void main(String[] ar){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array");
		int size = sc.nextInt();

		TwoStack obj = new TwoStack (size);

		char ch;

		do{
		
			System.out.println("1.push1");
			System.out.println("2.push2");
			System.out.println("3.pop1");
			System.out.println("4.pop2");

			int choice = sc.nextInt();
			switch(choice){
			
				case 1:
					{
					
						System.out.println("Enter element for stack1");
						int data = sc.nextInt();
						obj.push1(data);
					}
					break;

				case 2:
					{
					
						System.out.println("Enter element for stack2");
						int data = sc.nextInt();
						obj.push2(data);
					}
					break;

				case 3:
					{
					
						int ret = obj.pop1();

						if(ret!=-1)
						System.out.println(ret+" popped from stack1");
					}
					break;

				case 4:
					{
						int ret = obj.pop2();

						if(ret!=-1)
						System.out.println(ret+" popped from stack1");
					}
				default:
					System.out.println("Not valid");
			}

			System.out.println("Enter choice");
			ch=sc.next().charAt(0);
		}while(ch=='Y' || ch == 'y');
	}
}
