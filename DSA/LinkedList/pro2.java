
import java.util.*;

class LinkedList{

class Node{

	int data;
	Node next;

	Node(int data){
	
		this.data= data;
	}
}
	Node head =null;

	void addFirst(int data){
	
		Node temp = new Node(data);
		if(head==null){
		
			head=temp;
			temp.next=head;
		}else{
			temp.next=head;
			head=temp;
		}
	}

	void addLast(int data){
	
		 Node temp = new Node(data);
                if(head==null){

                        head=temp;
                }else{
		
			Node  p = head;
			
			while(p.next!=head){
			
				p=p.next;
			}

			p.next=temp;
		}
			temp.next=head;
	}

	int count(){
	
		Node temp = head;
		int count=0;

		while(temp.next!=head){
		
			temp=temp.next;
			count++;
		}

		return count+1;
	}

	void addAtPos(int pos,int data){
	
		int cnt = count();
		if(pos<0 || pos>cnt+1){
					
			System.out.println("Wrong input");
			return;
		}
		
		if(pos==1){
		
			addFirst(data);
		}else if(pos==cnt){
		
			addLast(data);
		}else{
		
			Node p= new Node(data);
			Node temp = head;
			while(pos-2>0){
			
				pos--;
				temp=temp.next;
			}
			p.next=temp.next;
			temp.next=p;
		}
	}

	void delLast(){
	
		if(head==null){
		
			System.out.println("Empty linkedlist");
			return;
		}
		Node temp=head;

		while(temp.next.next!=head){
		
			temp = temp.next;
		}
		temp.next=head;
	}

	void printSLL(){
	
		if(head==null){
		
			System.out.println("Empty LinkedList");
			return;
		}
		else{
		
			Node temp =head;

			while(temp.next!=head){
			
				System.out.print(temp.data +"->");
				temp=temp.next;
			}
				System.out.println(temp.data );
		}
	}

	void delFirst(){
	
		if(head==null){

                        System.out.println("Empty linkedlist");
                        return;
                }
		Node temp = head;
		while(temp.next!=head){
		
			temp=temp.next;
		}
		head=head.next;
		temp.next=head;

	}

	void delAtPos(int pos){

                int cnt = count();
                if(cnt<=0 || cnt>pos){

                        System.out.println("Wrong input");
			return;
                }else if(pos==1){
		
			delFirst();		
		}else{
                        Node temp = head;
                        while(pos-2>0){
                                pos--;
                                temp=temp.next;
                        }
                        temp.next=temp.next.next;
                }
        }

	void InPlace(){
	
		if(head==null){
		
			System.out.println("");
		}else if(count()==1){
		
			return;
		}else{
		
			Node temp = head;
			Node temp1=head;
			Node temp2=null;

			while(temp1.next!=head){
			
				temp1=temp1.next;								
			}

			while(temp.next!=head){
			
				temp2=temp;
				temp=temp.next;
				temp2.next=temp1;
				temp1=temp2;
			}
			 temp2=temp;
                                temp2.next=temp1;
				head=temp2;
		}
	}
}

class Demo{

	public static void main(String[] ar){
	
		LinkedList ll = new LinkedList();
		char p;

		do{
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.delLast");
			System.out.println("5.delAtPos");
			System.out.println("6.delFirst");
			System.out.println("7.print SLL");
			System.out.println("8.Inplace");
			System.out.println("Enter choice");

			Scanner sc = new Scanner(System.in);
			int ch= sc.nextInt();
			switch(ch){
			
				case 1:
					{
						System.out.println("Enter data");
						int data=sc.nextInt();
						ll.addFirst(data);
					}
					break;

				case 2:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();
						ll.addLast(data);
					}
					break;

				case 3:
					{
						System.out.println("Enter position");
						int pos =sc.nextInt();
						System.out.println("Enter data");
						int data = sc.nextInt();
                                                ll.addAtPos(pos,data);
					}
					break;

				case 4:
					ll.delLast();
					break;

				case 5:
					{
						System.out.println("Enter position");
						ll.delAtPos(sc.nextInt());
					}
					break;

				case 6:
					ll.delFirst();
					break;

				case 7:
					ll.printSLL();
					break;

				case 8:
					ll.InPlace();
					break;

				default:
					System.out.println("Invalid choice re-enter");
					break;
			}

			System.out.println("Do you want continue");
			p = sc.next().charAt(0);

		}while(p=='y' || p=='Y');
	}
}
