
import java.util.*;

class Node{

	int data;
	Node next;

	Node(int data,Node next){
	
		this.data=data;
		this.next=next;
	}
}

class LinkedList{

	Node head=null;

	void addLast(int data){

                 Node temp = new Node(data,null);
                if(head==null){

                        head=temp;
                }else{

                        Node  p = head;

                        while(p.next!=null){

                                p=p.next;
                        }

                        p.next=temp;
                }
        }
	void reverseRec(Node prev,Node curr){
	
		if(curr==null){
		
			head=prev;
			return;
		}else{
		
			Node forward=curr.next;
			curr.next=prev;
			prev=curr;
			curr=forward;
		}
		reverseRec(prev,curr );
	}

		void printLL(){
		
			Node temp = head;
			int y=7;

			while(temp.next!=null){
			
				System.out.print(temp.data+" -> ");
				temp=temp.next;
				if(y--<0)
					break;
			}
				System.out.println(temp.data+"-------------------------------------\n");
		}
	void kgroup(int n){
	
		Node temp1 =head;
		Node temp2= null;
		Node temp3=null;
		Node temp4=head;
		Node temp5=head;
		Node temp6=null;
		Node temp7=null;
		int tt=n-1;

		while(temp1!=null){
		
			while(tt-->0 && temp5!=null){
			
				temp5=temp5.next;
			}

			if(temp5!=null){
			temp6=temp5.next;
			temp5.next=null;
			
				while(temp1!=null){
				
					temp2=temp1.next;
					temp1.next=temp3;
					temp3=temp1;
					temp1=temp2;
				}

				if(temp4==head){
				
					head=temp5;
				}else{
				
					temp4.next=temp3;
					temp4=temp7;
				}
					temp7=temp6;

			}else{
			
				temp3=null;
				while(temp1!=null){
					temp2=temp1.next;
                                        temp1.next=temp3;
                                        temp3=temp1;
                                        temp1=temp2;
				}

				if(temp4==head)
					head=temp3;

				else{
				
					temp4.next=temp3;
					break;
				}
			}

			temp5=temp6;
			temp1=temp6;
			tt=n-1;

		}
	}
}

class Demo{

	public static void main(String[] ar){
	
		LinkedList ll = new LinkedList();

		ll.addLast(1);
		ll.addLast(2);
		ll.addLast(3);
		ll.addLast(4);

		ll.printLL();
		ll.kgroup(2);
		ll.printLL();
	}
}
