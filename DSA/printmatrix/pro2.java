
import java.io.*;

class Demo{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size");
		int arr[][]=new int[Integer.parseInt(br.readLine())][Integer.parseInt(br.readLine())];

		for(int i=0;i<arr.length;i++){
		
			for(int j=0;j<arr[i].length;j++){
			
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}

		for(int  i=0;i<arr.length;i++){
		
			for(int j=i+1;j<arr.length;j++){
			
				int temp = arr[i][j];
				arr[i][j]=arr[j][i];
				arr[j][i]=temp;
			}
		}
		for(int i=0;i<arr.length;i++){

                        for(int j=0;j<arr[i].length;j++){

                                System.out.print(arr[i][j]+" ");
                        }
			System.out.println();
                }
	}
}
