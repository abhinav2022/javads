
import java.io.*;

class Demo{

	public static void main(String[] ar) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int arr[] = new int[Integer.parseInt(br.readLine())];
		System.out.println("Enter array elements");
		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}

		int start =0,end=0,k,sum=0,maxele=Integer.MIN_VALUE;
		System.out.println("Enter size of subarray");
		k = Integer.parseInt(br.readLine());
		end=k-1;
		for(int i=start;i<=end;i++){
		
			sum = sum +arr[i];
		}

		start=1;
		end=k;

		while(end<arr.length){
		
			sum = sum -arr[start-1]+arr[end];
			if(sum>maxele)
				maxele=sum;

			start++;
			end++;
		}

		System.out.println("Max subarray sum is "+maxele);

	}
}
