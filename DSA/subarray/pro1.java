
import java.io.*;

class Demo{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number");

		int arr[] = new int[Integer.parseInt(br.readLine())];

		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		int k=0,max;
		System.out.println("Enter max element");
		max=Integer.parseInt(br.readLine());
		for(int i=0;i<arr.length;i++){
		
			if(max>=arr[i])
				k++;				
		}
		int min=Integer.MAX_VALUE;

		for(int i=0;i<arr.length-k+1;i++){
			
			int cnt=0;
			for(int j=i;j<i+k;j++){
			
				if(arr[j]<=max){
				
					cnt++;
				}
			}
			cnt=k-cnt;
			if(cnt<min)
				min=cnt;
		}

		System.out.println(min);
	}
}
