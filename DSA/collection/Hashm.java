
import java.util.*;

class Demo{

	public static void main(String[] ar){
	
		HashMap hm = new HashMap();

		hm.put(1,"one");
		hm.put(2,"two");
		hm.put(3,"three");
		hm.put(4,"four");

		System.out.println(hm);
		hm.put(1,"oe");
		System.out.println(hm);
		System.out.println(hm.get(1));
		System.out.println(hm.getOrDefault(1,"one"));
		System.out.println(hm);
	}
}
