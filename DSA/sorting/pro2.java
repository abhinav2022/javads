
class Demo{

	static int cnt=0;
	static void bubble(int arr[],int n){
	
		if(n==1)
			return;
		boolean swap=true;

		for(int j=0;j<n-1;j++){
		
			cnt++;
			if(arr[j]>arr[j+1]){
			
				int temp = arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
				swap=false;
			}
		}

		if(swap==false)
			bubble(arr,n-1);
	}
	public static void main(String[] ar){
	
		int arr[]= new int[]{5,4,3,2,1};
		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		bubble(arr,arr.length);
		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		System.out.println(cnt);
	}
}
