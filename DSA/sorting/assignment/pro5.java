
import java.util.*;

class Demo{

	void fun(int arr[],int start,int mid,int end){
	
		int l1=mid-start+1;
		int l2=end-mid;
		int arr1[]=new int[l1];
		int arr2[]=new int[l2];

		for(int i=0;i<arr1.length;i++){
		
			arr1[i]=arr[start+i];
		}
		for(int i=0;i<arr2.length;i++){
		
			arr2[i]=arr[mid+i+1];
		}

		int itr1=0,itr2=0,itr3=start;

		while(itr1<l1 && itr2<l2){
		
			if(arr1[itr1]<=arr2[itr2]){
			
				arr[itr3]=arr1[itr1];
				itr1++;
			}else{
			
				arr[itr3]=arr2[itr2];
				itr2++;
			}
			itr3++;
		}

		while(itr1<l1){
		
			 arr[itr3]=arr1[itr1];
                                itr1++;
				itr3++;
		}
		while(itr2<l2){

                         arr[itr3]=arr2[itr2];
                                itr2++;
                                itr3++;
                }

	}
void print(int arr[]){

                for(int i=0;i<arr.length;i++){

                        System.out.print(arr[i]+" ");
                }
                System.out.println();
        }
	void merge(int arr[],int start,int end){
	
		if(start<end){
		
			int mid=(start+end)/2;
			merge(arr,start,mid);
			merge(arr,mid+1,end);
			fun(arr,start,mid,end);
		}
	}

	public static void main(String[] a){

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter array size");
                int arr[]=new int[sc.nextInt()];
                System.out.println("Enter array element");
                for(int i=0;i<arr.length;i++){

                        arr[i]=sc.nextInt();
                }
                Demo d = new Demo();
                d.print(arr);
		d.merge(arr,0,arr.length-1);
                d.print(arr);


        }
}
