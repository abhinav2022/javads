
import java.util.*;

class Demo{

	void print(int arr1[],int arr2[]){

                for(int i=0;i<arr1.length;i++){

                        System.out.print(arr1[i]+" ");
                }
                System.out.println();
                for(int i=0;i<arr1.length;i++){

                        System.out.print(arr2[i]+" ");
                }
                System.out.println();

        }

	int part(int arr[],int start ,int end){
	
		int piv=arr[end];
		int i=start-1;
		for(int j=start;j<end;j++){
		
			if(arr[j]<piv){
			
				i++;

				int temp = arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}

		int temp = arr[++i];
		arr[i]=arr[end];
		arr[end]=temp;

		return i;	
	}

	void sort(int arr[],int start,int end){
	
		if(start<end){
		
			int piv=part(arr,start,end);

			sort(arr,start,piv-1);
			sort(arr,piv+1,end);
		}
	}

	void fun(int arr1[],int arr2[]){
	
		int i=0;
		for(int j=arr1.length-1;j>=0;j--){
		
			if(arr1[j]>arr2[i]){
			
				int temp=arr1[j];
				arr1[j]=arr2[i];
				arr2[i]=temp;
				i++;
			}else
				break;
		}
		
		sort(arr1,0,arr1.length-1);	
		sort(arr2,0,arr2.length-1);	
	}

        public static void main(String[] a){

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter array size");
                int arr1[]=new int[sc.nextInt()];
                System.out.println("Enter array size");
                int arr2[]=new int[sc.nextInt()];
                System.out.println("Enter array element");
                for(int i=0;i<arr1.length;i++){

                        arr1[i]=sc.nextInt();
                }
                System.out.println("Enter array element");
                for(int i=0;i<arr2.length;i++){

                        arr2[i]=sc.nextInt();
                }
                Demo d = new Demo();
                d.print(arr1,arr2);
                d.fun(arr1,arr2);
                d.print(arr1,arr2);
        }
}
