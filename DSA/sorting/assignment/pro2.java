
import java.util.*;

class Demo{

	void print(int arr[]){

                for(int i=0;i<arr.length;i++){

                        System.out.print(arr[i]+" ");
                }
                System.out.println();
        }
	void fun(int arr[]){
	
		int t0=0,t1=0,t2=0;

		for(int i=0;i<arr.length;i++){
		
			if(arr[i]==0)
				t0++;
			else if(arr[i]==1)
				t1++;
			else if(arr[i]==2)
				t2++;
			else{
			
				System.out.println("wrong array");
				return;
			}
		}

		for(int i=0;i<arr.length;i++){
		
			if(t0>0){
			
				arr[i]=0;
				t0--;
			}else if(t1>0){
			
				arr[i]=1;
				t1--;
			}else if(t2>0){
			
				arr[i]=2;
				t2--;
			}
		}
	}

        public static void main(String[] a){

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter array size");
                int arr[]=new int[sc.nextInt()];
                System.out.println("Enter array element");
                for(int i=0;i<arr.length;i++){

                        arr[i]=sc.nextInt();
                }
                Demo d = new Demo();
                d.print(arr);
                d.fun(arr);
                d.print(arr);
        }
}
