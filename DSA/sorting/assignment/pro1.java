
import java.util.*;

class Demo{

	public void bubble(int arr[],int i){
	
		boolean flag=false;
			for(int j=1;j<i-1;j++){
			
				if(arr[j-1]>arr[j]){
				
					int temp = arr[j-1];
					arr[j-1]=arr[j];
					arr[j]=temp;
					flag=true;
				}
			}

			if(flag){
			
				bubble(arr,i-1);
			}

	}

	void print(int arr[]){
	
		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
	
	public static void main(String[] a){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size");
		int arr[]=new int[sc.nextInt()];
		System.out.println("Enter array element");
		for(int i=0;i<arr.length;i++){
		
			arr[i]=sc.nextInt();
		}
		Demo d = new Demo();
		d.print(arr);
		d.bubble(arr,arr.length);
		d.print(arr);
	}
}
