
import java.io.*;

class Arrbasic{

	static int fun(int a){
	
		int temp=a;
		int si=0;
		while(temp>0){
		
			if(temp%10==0)
			si=si*10+5;

			else
				si=si*10+temp%10;
			temp/=10;
		}

		temp=si;
		si=0;
		while(temp>0){
		
			si=si*10+temp%10;
			temp/=10;
		}
		return si;
	}

	public static void main(String[] ar){
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try{
		
			int a = Integer.parseInt(br.readLine());
			
			System.out.println(fun(a));
		}catch(IOException i){
		
			System.out.println(i);
		}

	}
}
