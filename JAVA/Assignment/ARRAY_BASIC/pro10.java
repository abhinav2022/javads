
import java.io.*;

class Arrbasics{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		
		int arr[]=new int[a];
		System.out.println("Enter array elements");
		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		int max=0,temp=0,min=2000000000;
		
		for(int i=0;i<arr.length;i++){
		
			if(arr[i]>0){
			
				temp+=arr[i];
			}

			else{
			
				if(min>-arr[i])
					min=-arr[i];

			}
		}

		if(temp%2!=0){
		
			System.out.println("final op = "+temp);
		}
		else if((temp-min)%2!=0){
		
			System.out.println("final op = "+(temp-min));
		}else{
		
			System.out.println("final op = "+(-1));
		}
	}
}
