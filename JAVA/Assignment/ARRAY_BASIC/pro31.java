
import java.io.*;

class Demo{

	static int f(int p,int y){
	
		if(Math.abs(p-y)>1)
			return p-y;
		else //(Math.abs(p-y)<=1)
			return 0;
	}

	public static void main(String[] ar)throws IOException{

                BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter number");

                int a = Integer.parseInt(br.readLine());
                int arr[]=new int[a];

                System.out.println("Enter element");
                for(int i=0;i<a;i++){

                        arr[i]=Integer.parseInt(br.readLine());
                }
		int p=0,itr=0;
		for(int i=0;i<arr.length-1;i++){
		
			for(int j=i+1;j<arr.length;j++){
			
				p+=f(arr[j],arr[i]);
				itr++;
			}
		}
		System.out.println("p "+p+" itr "+itr);
	}
}
