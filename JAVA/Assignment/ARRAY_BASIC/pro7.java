
import java.io.*;

class Arrbasics{

	static void sorting(int arr[]){
	
		int max=arr[0];

		for(int i=1;i<arr.length;i++){
		
			if(max<arr[i]){
			
				max=arr[i];
			}
		}

		int count[]=new int[max+1];

		for(int i=0;i<arr.length;i++){
		
			count[arr[i]]=count[arr[i]]+1;
		}

		for(int i=1;i<count.length;i++){
		
			count[i]=count[i]+count[i-1];
		}
		
		int output[]=new int[arr.length];
		for(int i=arr.length-1;i>=0;i--){
		
			output[--count[arr[i]]] = arr[i];
		}
		for(int i=0;i<arr.length;i++){
		
			arr[i]=output[arr.length-1-i];
		}

	}

	static int fun(int arr[]){
	
		int max=0;
		for(int i=0;i<arr.length;i++){
		
			max=max*10+arr[i];
		}
		return max;
	}

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array");
		int a = Integer.parseInt(br.readLine());
		int arr[]= new int [a];
		System.out.println("Enter array elements");
		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		sorting(arr);
		System.out.println(fun(arr));
	}
}
