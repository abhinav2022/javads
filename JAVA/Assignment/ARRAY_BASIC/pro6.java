
import java.io.*;

class Arrbasics{

	public static void main(String[] a)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size\n");
		int size = Integer.parseInt(br.readLine());
		int arr1[]=new int[size];
		System.out.println("Enter array elements");
		for(int i=0;i<arr1.length;i++){
		
			arr1[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("----------------------");
		int flag0=0,flag1=0,flag2=0,se1,se2;
		se1=Integer.parseInt(br.readLine());
		se2=Integer.parseInt(br.readLine());
		for(int i=0;i<arr1.length;i++){
		
			if(arr1[i]==se1)
				flag0=1;
			else if(arr1[i]==se2)
				flag1=1;
			if(arr1[i]>se1 && arr1[i]<se2)
				flag2++;
		}
		
		if(flag0==1 && flag1==1 && flag2>0){
		
			System.out.println("Yes");
		}else{
		
			System.out.println("No");
		}
	}
}
