
import java.io.*;

class Arrbasics{

	public static void main(String[] ar) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a= Integer.parseInt(br.readLine());
		int arr[]=new int[a];
		System.out.println("Enter array elements");
		for(int i=0;i<arr.length;i++){
		
			arr[i]= Integer.parseInt(br.readLine());
		}
		System.out.println("Search ");
		int search = Integer.parseInt(br.readLine());
		int flag=0;
		for(int i=0;i<arr.length;i++){
		
			if(search==arr[i] && flag==0){
				System.out.println(i);
				flag=1;
			}
			if(search== arr[i] && arr[i]!=arr[i+1]){
				 System.out.println(i);
				 flag=2;
				 break;
			}
		}
		
		if(flag==1){
		
			System.out.println(arr.length-1);
		}
		if(flag==0)
			 System.out.println(-1);

	}
}
