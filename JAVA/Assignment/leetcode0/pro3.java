
import java.io.*;

class Demo{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number");
		int arr[]= new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter array element");

		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter search");
		int search = Integer.parseInt(br.readLine());

		int start =0,end=arr.length,mid=0;
		while(start<end){
		
			 mid = (start+end)/2;

			if(arr[mid]==search){
			
				start=mid;
				break;
			}

			else if(arr[mid]<search)
				start=mid+1;

			else if(arr[mid]>search)
				end=mid-1;
		}
		
		System.out.println(start);
	}
}
