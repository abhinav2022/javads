
import java.io.*;

class Demo{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());
		int temp=a,sum=0;
		if(a<0)
			temp=-a;
		while(temp>0){
		
			sum=sum*10+temp%10;
			temp/=10;
		}
		if(a<0)
			System.out.println(-sum);
		else
		System.out.println(sum);

	}
}
