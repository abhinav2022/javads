
import java.io.*;

class Demo{

	public static void main(String[] AR)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int arr[]=new int[Integer.parseInt(br.readLine())];
		System.out.println("Enter array elements");
		int cn1=0,cn2=0;
		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]==0)
				cn1++;
			else if(arr[i]==1)
				cn2++;
		}
		
		if(cn1>cn2){
		
			System.out.println(cn2);
		}else if(cn1<cn2){
		
			System.out.println(cn1);
		}else
			System.out.println(cn1+cn2);
	}
}
