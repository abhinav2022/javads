
import java.io.*;

class SwitchDemo{

	public static void main(String[] args)throws IOException{
	
		int a,b,c,d,e;
		float avg;
		System.out.println("Enter marks of subjects");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		a = Integer.parseInt(br.readLine());
		b = Integer.parseInt(br.readLine());
		c = Integer.parseInt(br.readLine());
		d = Integer.parseInt(br.readLine());
		e = Integer.parseInt(br.readLine());
		if(a<0 || b<0 || c<0 || d<0 ||e<0){
		
			System.out.println("Invalid marks");
			return;
		}
		if(a<40 || b<40 ||c<40 || d<40 || e<40){
		
			System.out.println("You failed in exam");
		}else{
		
			avg = (a+b+c+d+e)/5;

			char ch ;

			if(avg>=40 && avg<=50){
			
				ch = 'p';

			}else if(avg>=50 && avg<=60){
			
				ch = 'e';
			}else if(avg>=60 && avg<=70){
			
				ch = 'd';
			}else if(avg>=70 && avg<=80){
			
				ch = 'c';
			}else if(avg>=80 && avg<=90){
			
				ch = 'b';
			}else if(avg>=90 && avg<=100){
			
				ch ='a';
			}else{
			
				ch = 'f';
			}

			switch(ch){
			
				case 'a':
					System.out.println("Merit");
					break;

				case 'b':
					System.out.println("distinction");
					break;

				case 'c':
					System.out.println("first class");
					break;

				case 'd':
					System.out.println("second class ");
					break;

				case 'e':
					System.out.println("third class");
					break;

				case 'p':
					System.out.println("Pass");
					break;

				default:
					System.out.println("Invalid");
					break;
			}

		}
	}
}
