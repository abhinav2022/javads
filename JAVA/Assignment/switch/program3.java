import java.io.*;

class Q3{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)) ;

		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());

		if(a< 0 || b<0){
		
			System.out.println("Sorry negative number not allowed");
		}else{
		
			int g = (a*b)%2;

			switch(g){
			
				case 0:
					System.out.println("Even");
					break;

				case 1:
					System.out.println("Odd");
			}
		}
	}
}
