import java.io.*;

class Q2{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());

		if(a<0){
		
			System.out.println("Number is negative");
		}else{
		
			switch(a){
			
				case 0:
					System.out.println("Zero");
					break;

				case 1:
					System.out.println("One");
					break;

				case 2:
					System.out.println("Two");
					break;

				case 3:
					System.out.println("Three");
					break;

				case 4:
					System.out.println("Four");
					break;

				case 5:
					System.out.println("Five");
					break;

				default:
					System.out.println("Greater than 5");

			}
		}
	}
} 
