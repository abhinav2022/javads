
import java.io.*;

class Q8{

	public static void main(String [] ar)throws IOException{
	
		System.out.println("Enter number");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());

		for(int i=a;i<=b;i++){
			int sum=0,c=i;
			
			while(c>0){
			
				sum=sum*10+c%10;
				c/=10;
			}

			if(sum==i)
				System.out.print(i+" ");
		}
		System.out.println();
	}
}
