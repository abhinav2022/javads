
import java.io.*;

class Q9{

	public static void main(String [] ar)throws IOException{
	
		System.out.println("Enter number");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());

		for(int i=a;i<=b;i++){
			int sum=0,c=i;
			while(c>0){
				int l=1,m=c%10;
				while(m>0){
				
					l*=m--;
				}
				sum+=l;
				c/=10;
			}
				if(i==sum)
				System.out.print(sum+" ");
		}
		System.out.println();
	}
}
