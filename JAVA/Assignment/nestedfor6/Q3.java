
import java.io.*;

class Q3{

	public static void main(String [] ar)throws IOException{
	
		System.out.println("Enter number");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());

		for(int i=a;i*i<=b;i++){
				System.out.print(i*i+" ");
		}
		System.out.println();
	}
}
