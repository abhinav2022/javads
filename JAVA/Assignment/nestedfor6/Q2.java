
import java.io.*;

class Q2{

	public static void main(String [] ar)throws IOException{
	
		System.out.println("Enter number");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());

		for(int i=a;i<=b;i++){
			int cnt=0;
			for(int j=1;j<i;j++){
			
				if(i%j==0)
					cnt++;
			}

			if(cnt>1)
				System.out.print(i+" ");
		}
		System.out.println();
	}
}
