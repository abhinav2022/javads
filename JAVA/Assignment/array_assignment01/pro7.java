
import java.io.*;

class Demo{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int arr[]= new int[Integer.parseInt(br.readLine())];
		System.out.println("Enter array Elements");
		int max=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			arr[i]= Integer.parseInt(br.readLine());
			if(max<arr[i])
				max=i;
		}
		
		for(int i=max;i<arr.length;i++){
			boolean flag=true;
			for(int j=i+1;j<arr.length;j++){
			
				if(arr[i]>arr[j]){
					
					flag=false;
					break;
				}
			}
			if(flag){
			
				System.out.println("leader "+arr[i]);
			}
		}

	}
}
