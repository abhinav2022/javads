
import java.io.*;

class Demo{

	public static void main(String[] ae)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int arr[]=new int[Integer.parseInt(br.readLine())];

		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}

		int max=Integer.MIN_VALUE,cnt=0;

		for(int i=0;i<arr.length;i++){
		
			if(arr[i]>max)
				max=arr[i];
		}

		for(int i=0;i<arr.length;){
		
			if(arr[i]==max){
			
				i++;
			}else{
			
				arr[i]++;
				cnt++;
			}
		}

		System.out.println("count = "+cnt);
	}
}
