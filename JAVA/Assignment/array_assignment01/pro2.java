
import java.io.*;

class Demo{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter elements\n");
		
		for(int i=0;i<arr.length;i++){
		
			arr[i]= Integer.parseInt(br.readLine());
		}

		System.out.println("Enter search element\n");
		int ele = Integer.parseInt(br.readLine()),cnt=0;
		
		for(int i=0;i<arr.length;i++){
		
			if(arr[i]==ele)
				cnt++;
		}

		System.out.println("Element occurence is "+cnt);
	}

	void fun(){
	
		System.out.println("Efun");
	}
}
