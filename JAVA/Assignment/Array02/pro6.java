
import java.io.*;

class Q6{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number");
		int a = Integer . parseInt(br.readLine());
		int arr[]=new int[a];

		for(int i=0;i<a;i++){
		
			 arr[i] = Integer.parseInt(br.readLine());
		}
		int Ecnt =arr[0];

		for(int i=1;i<a;i++){
			
			if(Ecnt<arr[i]){
				Ecnt=arr[i];
			}
		}
		System.out.println("Max element is "+Ecnt);
	}
}
