
import java.io.*;

class Q9{

	public static void main(String[] ar)throws IOException{
	
		System.out.println("Enter number\n");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());

	       int arr1[]=new int[a];
	       int arr2[]=new int[b];	
	       int arr3[]=new int[a+b];
		int l=0;
		System.out.println("Enter number in array1\n");
	       for(int i=0;i<a;i++){
	       
		       arr1[i]=Integer.parseInt(br.readLine());
		       arr3[l++]=arr1[i];
	       }
		System.out.println("Enter number in array2\n");
	       for(int i=0;i<b;i++){
	       
		       arr2[i]=Integer.parseInt(br.readLine());
		       arr3[l++]=arr2[i];
	       }

		for(int i=0;i<a+b;i++){
		
			System.out.print(arr3[i]+" ");
		}
		System.out.println();
	}
}
