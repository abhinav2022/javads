
import java.io.*;

class Q2{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number");
		int a = Integer . parseInt(br.readLine());
		int arr[]=new int[a];
		int Ecnt =0,Ocnt=0;

		for(int i=0;i<a;i++){
		
			 arr[i] = Integer.parseInt(br.readLine());
			 if(arr[i]%2==0)
			 	Ecnt++;
			 else
				 Ocnt++;
		}

		System.out.println("Even number count :- "+Ecnt);
		System.out.println("Odd number count :- "+Ocnt);
	}
}
