
import java.io.*;

class Q10{

	public static void main(String[] ar)throws IOException{
	
		System.out.println("Enter number\n");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());

	       int arr1[]=new int[a];
		int l=0;
		System.out.println("Enter number in array1\n");
	       for(int i=0;i<a;i++){
	       
		       arr1[i]=Integer.parseInt(br.readLine());
	       }
		System.out.println();

		for(int i=0;i<a;i++){
		
			int sum=0;
			l=arr1[i];

			while(l>0){
			
				sum = sum + l%10;
				l/=10;
			}

			if(sum%2==0){
			
				System.out.println(arr1[i]);
			}
					
		}
		System.out.println();
	}
}
