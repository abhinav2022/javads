
import java.io.*;

class Q1{

	public static void main(String[]args )throws IOException{
	
		System.out.println("Enter number");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());

		System.out.println("Enter number");
		int arr[] = new int[a];
		int cnt=0;

		for(int i=0;i<a;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2!=0)
				cnt+=arr[i];
		}

			System.out.println("sum of odd is "+cnt);	
		}
}
