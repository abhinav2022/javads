
import java.io.*;

class Q5{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());

		int arr[]=new int [a];


		for(int i=0;i<a;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		}

		for(int i=0;i<a;i++){
		
			int p = arr[i];
			int cnt=0;

			for(int j=1;j<p;j++){
			
				if(p%j==0)
					cnt+=j;
			}

			if(cnt==p)
			System.out.println("Perfect number "+arr[i]+" found at "+i);
		}
	}
}
