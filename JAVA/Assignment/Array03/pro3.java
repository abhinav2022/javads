
import java.io.*;

class Q3{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());

		int arr[]=new int [a];


		for(int i=0;i<a;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		}

		for(int i=0;i<a;i++){
		
			int p = arr[i];
			int cnt=0;

			for(int j=1;j<p;j++){
			
				if(arr[i]%i==0)
					cnt++;
			}

			if(cnt>1)
			System.out.println("Composite "+arr[i]+" number found at "+i);
		}
	}
}
