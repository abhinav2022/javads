
import java.io.*;

class Q7{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());

		int arr[]=new int [a];


		for(int i=0;i<a;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		}

		for(int i=0;i<a;i++){
			int l=arr[i];
		int sum =0;
			while(l>0){
			int p = l%10;
			int cnt=1;

			while(p>0){
				cnt*=p;
				p--;
			}
			sum+=cnt;
			l/=10;
			}
			if(sum==arr[i])
			System.out.println("Perfect number "+arr[i]+" found at "+i);
		}
	}
}
