
import java.io.*;

class Q6{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());

		int arr[]=new int [a];


		for(int i=0;i<a;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		}

		for(int i=0;i<a;i++){
		
			int p = arr[i];
			int cnt=0;

			while(p>0){
			
				cnt=cnt*10+p%10;
				p/=10;
			}

			if(cnt==arr[i])
			System.out.println("Perfect number "+arr[i]+" found at "+i);
		}
	}
}
