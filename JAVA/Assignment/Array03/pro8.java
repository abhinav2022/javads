
import java.io.*;

class Q7{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());

		int arr[]=new int [a];


		for(int i=0;i<a;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		}

		for(int i=0;i<a;i++){
			int l=arr[i];
		int sum =0,cnt=0,k=1;
			while(l>0){
			
				cnt++;
				l/=10;
			}
			l=arr[i];
			while(l>0){
				int q=cnt;
				while(q>0){
				
					k*=l%10;
					q--;
				}
			sum+=k;
			l/=10;
			k=1;
			}
		
			if(sum==arr[i])
			System.out.println("Perfect number "+arr[i]+" found at "+i);
		}
	
	}
}
