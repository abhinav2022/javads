
import java.util.*;

class Demo implements Comparable {

	String name=null;
	int x=0;
	Demo(String name,int x){
	
		this.name=name;
		this.x=x;
	}

	public int compareTo(Object obj1){
	
		return name.compareTo( ((Demo)obj1).name );
	}

	public String toString(){
	
		return name;
	}
};

class TreeSetDemo{

	public static void main(String[] ar){
	
		TreeSet ts = new TreeSet();

		ts.add(new Demo("Virat",18));
		ts.add(new Demo("Virat",18));

		System.out.println(ts);
	}
}
