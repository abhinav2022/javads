
import java.util.concurrent.*;

class MyThread implements Runnable{

	int num;

	MyThread(int num){
	
		this.num=num;
	}

	public void run(){
	
		System.out.println(Thread.currentThread()+" start "+num);
		task();
		System.out.println(Thread.currentThread()+" end "+num);
	}

	void task(){
	
		try{
		
			Thread.sleep(2000);

		}catch(InterruptedException r) {
		

		}
	}
}

class ThreadPoolDemo{

	public static void main(String[] ar){
	
		ExecutorService ser = Executors.newCachedThreadPool();

		System.out.println("cachedpool");
		for(int i=0;i<=6;i++){
		
			MyThread obj = new MyThread(i);

			ser.execute(obj);
		}
		ser.shutdown();
	}
}
