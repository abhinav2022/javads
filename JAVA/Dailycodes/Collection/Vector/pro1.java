
import java.util.*;

class MyThread implements Runnable{

	Vector v;

	MyThread (Vector v){
	
		this.v = v;
	}

	public synchronized void run(){
	
		System.out.println(Thread.currentThread().getName());

		for(int i=0;i<=5;i++){
		
			System.out.println("Inserted by Thread: "+Thread.currentThread().getName());
			v.addElement(i);
		}
		Enumeration cursor = v.elements();
		while(cursor.hasMoreElements()){

                        System.out.println("output by Thread: "+Thread.currentThread().getName()+" " +cursor.nextElement());
                }
		//System.out.println("output by Thread: "+Thread.currentThread().getName()+" "+v);
	}
}

class VectorDemo{

	public static void main(String[] ar){
	
		Vector v = new Vector();
		MyThread obj = new MyThread(v);

		Thread t1 = new Thread(obj);
		Thread t2 = new Thread(obj);
		Thread t3 = new Thread(obj);

		t1.start();
		t2.start();
		t3.start();
	}
}
