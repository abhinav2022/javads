
import java.util.*;

class VectorDemo{

	public static void main(String[]ar){
	
		Vector v = new Vector();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);

		System.out.println(v);
		System.out.println();

		Enumeration cursor = v.elements();

		while(cursor.hasMoreElements()){
		
			System.out.println(cursor.nextElement());
		}
	}
}
