
import java.util.*;

class ArrayListDemo{

	public static void main(String[] ar){
	
		ArrayList al = new ArrayList();

		al.add("Kanha");
		al.add("Rahul");
		al.add("Ashish");

		Iterator itr = al.iterator();

		while(itr.hasNext()){
		
			if("Rahul".equals(itr.next())){
			
				itr.remove();
			}
		}
		System.out.println(al);
	}
}
