
import java.util.*;

class CricPlayer{

	int jerNo = 0;
	String name= null;

	CricPlayer(int jerNo,String name){
	
		this.jerNo = jerNo;
		this.name = name;
	}

	public String toString(){
	
		return jerNo+": "+name;
	}
}

class ArrayListDemo{

	public static void main(String[] ar){
	
		ArrayList al = new ArrayList();

		al.add(new CricPlayer(18,"virat"));
		al.add(new CricPlayer(7,"Dhoni"));
		al.add(new CricPlayer(45,"Rohit"));

		System.out.println(al);

		//var l = al;

		for(var l : al){
		
			System.out.println(l.getClass().getName());

		}

		System.out.println("\n");

		Iterator itr = al.iterator();
		while(itr.hasNext()){
		
//			if("Rahul".equals(itr.next()))
//				itr.remove();

				System.out.println(itr.next());
		}
	}
} 
