
import java.util.*;

class ITCO{

	String str=null;
	int id = 0;

	ITCO(String str,int id){
	
		this.str=str;
		this.id= id;
	}

	public String toString(){
	
		return str + ": " +id;
	}
}

class LLDemo{

	public static void main(String[] ar){
	
		LinkedList ll = new LinkedList();

		ll.add(new ITCO("XYZ",2));
		ll.addFirst(new ITCO("ABC",1));
		ll.addLast(new ITCO("Pqe",3));

		System.out.println(ll);
		System.out.println(ll.peek());
		System.out.println(ll.node(0));
	}
}
