
import java.util.*;

class CricPlayer{

	int jerNo = 0;
	String name= null;

	CricPlayer(int jerNo,String name){
	
		this.jerNo = jerNo;
		this.name = name;
	}

	public String toString(){
	
		return jerNo+": "+name;
	}
}

class ArrayListDemo{

	public static void main(String[] ar){
	
		ArrayList al = new ArrayList();

		al.add(new CricPlayer(18,"virat"));
		al.add(new CricPlayer(7,"Dhoni"));
		al.add(new CricPlayer(45,"Rohit"));

		System.out.println(al);

		//var l = al;
		Iterator itr = al.iterator();
	
		System.out.println(itr.next());
		while(itr.hasNext()){

			System.out.println(itr.next());			
		}
		System.out.println();

		ListIterator litr = al.listIterator();

		while(litr.hasNext()){
		
			System.out.println(litr.next());
		}

		while(litr.hasprevious()){
		
			System.out.println(litr.previous());
		}
	}
} 
