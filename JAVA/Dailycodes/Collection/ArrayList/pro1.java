
import java.util.*;

class Movies{

	String Mname=null;
	float imdb=0.0f;
	double coll=0.0;

	Movies(String Mname,float imdb,double coll){
	
		this.Mname = Mname;
		this.imdb =imdb;
		this.coll = coll;
	}

	public String toString(){
	
		return "{ "+Mname +":"+imdb+":"+coll+" }";
	}
}

class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ( ((Movies)obj1).Mname.compareTo(((Movies)obj2).Mname));
	}
}

class SortByimdb implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		int p= (int)( ((Movies)obj1).imdb -     (((Movies)obj2).imdb) );

		System.out.println(p+" compare "+(((Movies)obj1).imdb - (((Movies)obj2).imdb))+((Movies)obj1).Mname+" "+((Movies)obj2).Mname);
		return p;
	}
}
class ArrayListDemo{

	public static void main(String[] ar){
	
		ArrayList al = new ArrayList();

		al.add(new Movies("Gadar2",7.0f,150));
		al.add(new Movies("RHTDM",8.8f,200));
		al.add(new Movies("Ved",7.5f,75));
		al.add(new Movies("Sairat",7.4f,100));

		System.out.println(al);

		Collections.sort(al,new SortByimdb());
		System.out.println(al);
		Collections.sort(al,new SortByName());
		System.out.println(al);

	}
}
