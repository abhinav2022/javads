
import java.util.*;

class Demo implements Comparable{

	int x=0;
	String str=null;
	Demo(String str , int x){
	
		this.str=str;
		this.x=x;
	}

	public String toString(){
	
		return str;
	}
	public int compareTo(Object obj){

		int y=(this.str).compareTo(((Demo)obj).str);
		System.out.println(y);	
		return (this.str).compareTo(((Demo)obj).str);
	}
}

class LHSDemo{

	public static void main(String[] ar){
	
		LinkedHashSet lhs = new LinkedHashSet();

		lhs.add(new Demo("MSD",7));
		lhs.add(new Demo("Virat",18));
		lhs.add(new Demo("Rohit",45));
		lhs.add(new Demo("MSD",7));
		System.out.println(lhs);
	}
}
