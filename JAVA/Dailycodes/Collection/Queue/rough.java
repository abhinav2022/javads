import java.util.*;
class PL implements Comparable{

	int x;
	PL(int y){
	
		this.x=y;
	}
	public int compareTo(Object obj){
	
		return ( x- ((PL)obj).x );
	}

	public String toString(){
	
		return x+"";
	}
}

class Sortp implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ( ((PL)obj1).x - ((PL)obj2).x );
	}
}

class Demo{

	public static void main(String[] ar){
	
		ArrayList al = new ArrayList ();

		al.add(new PL(3));
		al.add(new PL(2));
		al.add(new PL(1));
		Collections.sort(al,new Sortp());
		System.out.println(al);
	}
}
