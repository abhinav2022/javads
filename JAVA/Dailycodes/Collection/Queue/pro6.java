
import java.util.*;
import java.util.concurrent.*;

class Demo{

	public static void main(String[] ar) throws InterruptedException{
	
		BlockingQueue bq = new ArrayBlockingQueue(3);

		bq.offer(10);
		bq.offer(20);
		bq.offer(39);

		System.out.println(bq);

		bq.put(49);
		System.out.println(bq);
	}
}
