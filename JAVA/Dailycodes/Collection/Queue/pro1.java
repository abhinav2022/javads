/*transient java.lang.Object[] elements;
  transient int head;
  transient int tail;
  public java.util.ArrayDeque();
  public java.util.ArrayDeque(int);
  public java.util.ArrayDeque(java.util.Collection<? extends E>);
  static final int inc(int, int);
  static final int dec(int, int);
  static final int inc(int, int, int);
  static final int sub(int, int, int);
  static final <E> E elementAt(java.lang.Object[], int);
  static final <E> E nonNullElementAt(java.lang.Object[], int);
  public void addFirst(E);
  public void addLast(E);
  public boolean addAll(java.util.Collection<? extends E>);
  public boolean offerFirst(E);
  public boolean offerLast(E);
  public E removeFirst();
  public E removeLast();
  public E pollFirst();
  public E pollLast();
  public E getFirst();
  public E getLast();
  public E peekFirst();
  public E peekLast();
  public boolean removeFirstOccurrence(java.lang.Object);
  public boolean removeLastOccurrence(java.lang.Object);
  public boolean add(E);
  public boolean offer(E);
  public E remove();
  public E poll();
  public E element();
  public E peek();
  public void push(E);
  public E pop();
  boolean delete(int);
  public int size();
  public boolean isEmpty();
  public java.util.Iterator<E> iterator();
  public java.util.Iterator<E> descendingIterator();
  public java.util.Spliterator<E> spliterator();
  public void forEach(java.util.function.Consumer<? super E>);
  public boolean removeIf(java.util.function.Predicate<? super E>);
  public boolean removeAll(java.util.Collection<?>);
  public boolean retainAll(java.util.Collection<?>);
  public boolean contains(java.lang.Object);
  public boolean remove(java.lang.Object);
  public void clear();
  public java.lang.Object[] toArray();
  public <T> T[] toArray(T[]);
  public java.util.ArrayDeque<E> clone();
  void checkInvariants();
  public java.lang.Object clone() throws java.lang.CloneNotSupportedException*/
import java.util.*;

class Demo{

	public static void main(String[] ar){
	
		ArrayDeque ad = new ArrayDeque();

		ad.add(1);
		ad.add(2);
		ad.add(3);
		ad.add(4);
		System.out.println(ad);

		System.out.println(ArrayDeque.inc(2,3,4));
	}
}
