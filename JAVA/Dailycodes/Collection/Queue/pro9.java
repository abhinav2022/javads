
import java.util.*;
import java.util.concurrent.*;

class Producer implements Runnable{

	BlockingQueue lbq;

	Producer(BlockingQueue lbq){
	
		this.lbq = lbq;
	}

	public void run(){
	
		for(int i=1;i<=10;i++){
		
			try{
			
				
				lbq.put(i);
				System.out.println("produced "+i);
			}catch(InterruptedException ie){}

			try{
			
				Thread.sleep(2000);
			}catch(InterruptedException ie){}
		}
	}
}

class Consumer implements Runnable{

	 BlockingQueue lbq;

        Consumer(BlockingQueue lbq){

                this.lbq = lbq;
        }

        public void run(){

                for(int i=1;i<=10;i++){

                        try{


                                        lbq.take();
                                        System.out.println("consumed "+i);
                        }catch(InterruptedException ie){}

                        try{

                                Thread.sleep(6000);
                        }catch(InterruptedException ie){}
                }
        }
}
class Demo{

	public static void main(String[] ar){
	
		BlockingQueue bq = new ArrayBlockingQueue(3);

		Producer p = new Producer(bq);
		Consumer c = new Consumer(bq);
		Thread t1 =new Thread(p);
		Thread t2 = new Thread(c);

		t1.start();
		t2.start();

	}
}
