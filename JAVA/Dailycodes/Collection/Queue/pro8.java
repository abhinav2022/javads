
import java.util.*;
import java.util.concurrent.*;

class Demo{

	public static void main(String[] ar) throws InterruptedException{
	
		BlockingQueue bq = new LinkedBlockingQueue();
		System.out.println(bq.size());

		bq.offer(10);
		bq.offer(20);
		bq.offer(39);

		System.out.println(bq);

		bq.offer(49,5,TimeUnit.SECONDS);
		System.out.println(bq);
		bq.offer(59);
		System.out.println(bq.size());
		bq.take();
		System.out.println(bq);
/*
		ArrayList al = new ArrayList();

		System.out.println("Arraylist "+al);

		bq.drainTo(al);

		System.out.println("ArrayList "+al);
		System.out.println(bq);
		System.out.println(bq.size());*/
	}
}
