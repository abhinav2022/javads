
import java.util.*;
import java.util.concurrent.*;

class Demo1 implements Comparable{

	int x;
	Demo1(int x){
	
		this.x=x;
	}

	public int compareTo(Object obj){

		System.out.println("In compareto " +this.x +"  "+ ((Demo1)obj).x+" "+(this.x - ((Demo1)obj).x));
		return this.x - ((Demo1)obj).x;
	}

	public String toString(){
	
		return x+"";
	}
}

class Demo{

	public static void main(String[] ar){
	
		BlockingQueue  bq = new PriorityBlockingQueue(6);

		System.out.println(bq.size());
		bq.offer(new Demo1(5));
		bq.offer(new Demo1(1));
		bq.offer(new Demo1(3));
		bq.offer(new Demo1(2));

		System.out.println(bq);
		bq.offer(new Demo1(-1));
		bq.offer(new Demo1(10));
		System.out.println(bq);
		System.out.println(bq.size());
	}
}
