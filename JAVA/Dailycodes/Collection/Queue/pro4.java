
import java.util.*;

class DequeD{

	public static void main(String[] ar){

		Deque obj = new ArrayDeque();

		obj.offer(10);
		obj.offer(40);
		obj.offer(20);
		obj.offer(30);

		System.out.println(obj);
		System.out.println(obj.offerFirst(3));
		System.out.println(obj.offerLast(4));
		System.out.println(obj);
		System.out.println(obj.pollFirst());
		System.out.println(obj.pollLast());
		System.out.println(obj.peekFirst());
		System.out.println(obj.peekLast());
		System.out.println(obj);

		Iterator itr = obj.iterator();
		while(itr.hasNext()){
		
			System.out.println(itr.next());
		}
		System.out.println(obj);
		Iterator itr1 = obj.descendingIterator();
		while(itr1.hasNext()){
		
			System.out.println(itr1.next());
		}
		System.out.println(obj);

	}
}
