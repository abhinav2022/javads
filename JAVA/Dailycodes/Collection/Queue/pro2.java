
import java.util.*;

class Proj implements Comparable{

	String name=null;
	int team=0;
	int duration=0;
	Proj(String name,int team,int duration){
	
		this.name= name;
		this.team=team;
		this.duration=duration;
	}

	public int compareTo(Object obj1){
	
		int y=name.compareTo(((Proj)obj1).name);
		System.out.println("432  "+y);

		return y;//name.compareTo(((Proj)obj1).name);
	}

	public String toString(){
	
		return name+":"+duration;
	}
}

class SortbyD implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		int y= ( ((Proj)obj1).duration - ((Proj)obj2).duration );
		System.out.println("432  "+y);
		return y;
	}
}

class SortbyName implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		int y= (((Proj)obj1).name).compareTo (((Proj)obj2).name );
		System.out.println("431  "+y);
		return y;
	}
}
class PQ{

	public static void main(String[] ar){
	
		PriorityQueue pq = new PriorityQueue(3,new SortbyD());

		pq.add(new Proj("ABC",3,40));
		pq.add(new Proj("GDE",3,90));
		pq.add(new Proj("BG",3,60));
		System.out.println(pq);
	}
}
