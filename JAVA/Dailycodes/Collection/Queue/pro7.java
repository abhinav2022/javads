
import java.util.*;
import java.util.concurrent.*;

class Demo{

	public static void main(String[] ar) throws InterruptedException{
	
		BlockingQueue bq = new ArrayBlockingQueue(3);
		System.out.println(bq.size());

		bq.put(10);
		bq.put(20);
		bq.put(39);

		System.out.println(bq);

		bq.offer(49,5,TimeUnit.SECONDS);
		System.out.println(bq);
		bq.take();
		bq.offer(49,5,TimeUnit.SECONDS);
		bq.offer(49,5,TimeUnit.SECONDS);
		System.out.println(bq);

		ArrayList al = new ArrayList();

		System.out.println("Arraylist "+al);

		bq.drainTo(al);

		System.out.println("ArrayList "+al);
		System.out.println(bq);
		System.out.println(bq.size());
	}
}
