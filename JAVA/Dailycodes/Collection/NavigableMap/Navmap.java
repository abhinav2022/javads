/* public abstract java.util.Map$Entry<K, V> lowerEntry(K);
  public abstract K lowerKey(K);
  public abstract java.util.Map$Entry<K, V> floorEntry(K);
  public abstract K floorKey(K);
  public abstract java.util.Map$Entry<K, V> ceilingEntry(K);
  public abstract K ceilingKey(K);
  public abstract java.util.Map$Entry<K, V> higherEntry(K);
  public abstract K higherKey(K);
  public abstract java.util.Map$Entry<K, V> firstEntry();
  public abstract java.util.Map$Entry<K, V> lastEntry();
  public abstract java.util.Map$Entry<K, V> pollFirstEntry();
  public abstract java.util.Map$Entry<K, V> pollLastEntry();
  public abstract java.util.NavigableMap<K, V> descendingMap();
  public abstract java.util.NavigableSet<K> navigableKeySet();
  public abstract java.util.NavigableSet<K> descendingKeySet();
  public abstract java.util.NavigableMap<K, V> subMap(K, boolean, K, boolean);
  public abstract java.util.NavigableMap<K, V> headMap(K, boolean);
  public abstract java.util.NavigableMap<K, V> tailMap(K, boolean);
  public abstract java.util.SortedMap<K, V> subMap(K, K);
  public abstract java.util.SortedMap<K, V> headMap(K);
  public abstract java.util.SortedMap<K, V> tailMap(K);*/
import java.util.*;

class NavMapDemo{

	public static void main(String[] ar){
	
		NavigableMap nm = new TreeMap();
		
		nm.put(1,"Ashish");
		nm.put(2,"Kanha");
		nm.put(3,"Badhe");
		nm.put(4,"Rahul");
		nm.put(5,"KMS");

		System.out.println(nm);
		System.out.println(nm.lowerKey(3));
		System.out.println(nm.lowerEntry(3));
		System.out.println(nm.floorKey(3));
		System.out.println(nm.floorEntry(3));
		System.out.println(nm.ceilingEntry(3));
		System.out.println(nm.ceilingKey(3));
		System.out.println(nm.higherEntry(3));
		System.out.println(nm.higherKey(3));

		System.out.println("------------------------------------------------");
		System.out.println(nm.firstEntry());
		System.out.println(nm.lastEntry());
		System.out.println(nm.pollFirstEntry());
		System.out.println(nm.pollLastEntry());
		System.out.println(nm);
	}
}
