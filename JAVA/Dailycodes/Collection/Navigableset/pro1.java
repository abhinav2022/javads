
import java.util.*;

class Demo{

	public static void main(String[] ar){
	
		NavigableSet ns = new TreeSet();

		ns.add("Jawa");
		ns.add("Harley");
		ns.add("Enfield");
		ns.add("KTM");
		ns.add("Ducati");

		System.out.println(ns);
		System.out.println(ns.floor("Enfield"));
		System.out.println(ns);
		System.out.println(ns.ceiling("Enfield"));
		System.out.println("---------------------------------------------------");
		System.out.println(ns.higher("KTM"));
		System.out.println(ns);
		System.out.println("---------------------------------------------------");

		System.out.println(ns.higher("Harley"));
		System.out.println(ns);
		System.out.println("---------------------------------------------------");
		System.out.println(ns.lower("Harley"));
		System.out.println(ns);

		System.out.println("---------------------------------------------------");
		System.out.println(ns.pollFirst());
		System.out.println(ns.pollLast());
		System.out.println(ns);
		System.out.println("---------------------------------------------------");

		System.out.println(ns.descendingSet());
		System.out.println(ns);
	}
}
