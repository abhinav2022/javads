
import java.util.*;

class Demo{

	public static void main(String[] ar){
	
		Dictionary d = new Hashtable();

		d.put(1,"qwe");
		d.put(2,"qwer");
		System.out.println(d);

		Enumeration en = d.keys();
		while(en.hasMoreElements()){
			
			System.out.println(en.nextElement());
		}

		System.out.println(d.get(1));
		System.out.println(d.remove(2));
		System.out.println(d);
	}
}
