
import java.util.*;

class WD{

	int y;
	WD(int y){
	
		this.y=y;
	}

	public String toString(){
	
		return ""+y;
	}
}

class Sortbyn implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ( ( (WD)obj1 ).y - ((WD)obj2).y );
	}
}

class Demo {

	public static void main(String[] ar){

		TreeMap hm = new TreeMap(new Sortbyn());

		hm.put(new WD(1),111);
		hm.put(new WD(7),11);
		hm.put(new WD(13),22);
		hm.put(new WD(32),44);
		hm.put(new WD(23),4);
		hm.put(new WD(34),44);
		hm.put(new WD(10),2);
		System.out.println();
		System.out.println(hm);
		HashMap tm = new HashMap(hm);

		System.out.println(hm);
		System.out.println(tm);
		HashMap hm1 = new HashMap();
		hm1.put(new WD(1),111);
		hm1.put(new WD(7),11);
		hm1.put(new WD(13),22);
		hm1.put(new WD(32),44);
		hm1.put(new WD(23),4);
		hm1.put(new WD(34),44);
		hm1.put(new WD(10),2);

		System.out.println(hm1);

		TreeMap tm1 = new TreeMap();

		tm1.put(1,111);
		tm1.put(7,13);
		tm1.put(13,22);
		tm1.put(32,44);
		tm1.put(23,4);
		tm1.put(34,44);
		tm1.put(10,2);

		System.out.println(tm1);

		HashMap hm2 = new HashMap(tm1);

		System.out.println(hm2);

	}
}
