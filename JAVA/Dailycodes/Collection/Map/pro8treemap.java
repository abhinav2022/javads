
import java.util.*;

class Platform{

	String str=null;

	Platform(String str){
	
		this.str=str;
	}

	public String toString(){
	
		return str;
	}
}

class Sortbyname implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ( (((Platform)obj1).str).compareTo(((Platform)obj2).str ) );
	}
}

class Demo{

	public static void main(String[] ar){
	
		TreeMap tm = new TreeMap(new Sortbyname());
		
		tm .put(new Platform("Youtube"),2005);
		tm.put(new Platform("facebook"),2004);
		tm.put(new Platform("Instagram"),2010);
		tm.put(new Platform("ChapGpt"),2022);

		System.out.println(tm);
	}
}
