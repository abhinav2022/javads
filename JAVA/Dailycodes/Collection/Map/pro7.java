
import java.util.*;

class Demo{

	String str;
	Demo(){}
	Demo(String str){
	
		this.str = str;
	}

	public String toString(){
	
		return str;
	}
	public void fun() throws Exception{}
	public void finalize() {
	
		System.out.println("Notify");
	}
}

class WDemo extends Demo{

	public void fun() {}

	public static void main(String[] ar){
	
		Demo obj1 = new Demo("C2W");
		Demo obj2 = new Demo("Biencaps");
		Demo obj3 = new Demo("Incubator");

		WeakHashMap whm = new WeakHashMap();
		whm.put(obj1,2016);
		whm.put(obj2,2019);
		whm.put(obj3,2023);
		
		System.out.println(obj1);
		System.out.println(obj2);
		System.out.println(obj3);
		System.out.println(whm);

		obj1=null;
		obj2=null;

		System.gc();
		System.out.println(whm);
		System.out.println("In main");
	}
}
