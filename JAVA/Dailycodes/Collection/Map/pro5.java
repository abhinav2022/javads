
import java.util.*;

class HashMapDemo{

	public static void main(String[] ar){

		HashMap hm = new HashMap();
		hm.put("Java",".java");
		hm.put("Python",".py");
		hm.put("Dart",".dart");
		System.out.println(hm);

		System.out.println(hm.get("Python"));
		System.out.println(hm.keySet());
		System.out.println(hm.values());
		Integer p = new Integer(1);
		System.out.println(System.identityHashCode(p));
		System.out.println(System.identityHashCode(p));
		System.out.println(p.hashCode());
	}
}
