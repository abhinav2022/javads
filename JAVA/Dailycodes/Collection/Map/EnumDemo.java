
import java.util.*;

enum colors{

	Red,
	Green,
	Blue;
}

class Demo{

	public static void main(String[] ar){
	
		EnumMap em = new EnumMap(colors.class);
		System.out.println(em);
		em.put(colors.Red,10);
		System.out.println(em);
	}
}
