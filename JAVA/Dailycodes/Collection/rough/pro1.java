
import java.util.*;

class PL{

	String name=null;
	int x;
	PL(String name , int n){
	
		this.name=name;
		this.x=n;
	}

	public String toString(){
	
		return name;
	}
	
}

class Demo{

	public static void main(String[] ar){
	
		HashMap hm = new HashMap();
		hm.put(new PL("ed",23),"1");
		hm.put(new PL("ad",34),"2");
		hm.put(new PL("ed",23),"3");
		hm.put("qwerty",122);
		hm.put("qwerty",123);

		System.out.println(hm.get("ed"));
		System.out.println(hm);
	}
}
