
import java.util.*;

class ITCOMP{

	String str = null;
	int id=0;

	ITCOMP(String str,int id){
	
		this.str=str;
		this.id=id;
	}
	public String toString(){
	
		return str +": "+id;
	}
}

class ArrayListDemo extends ArrayList{

	public static void main(String[] args){
	
		ArrayListDemo al = new ArrayListDemo();
		//add(Element )
		al.add(10);
		al.add(20.5f);
		al.add("Abhinav");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);
		
		//size()
		System.out.println(al.size());

		//contain(Object)
		System.out.println(al.contains("Abhinav"));
		System.out.println(al.contains(30));

		//indexOf(element)
		System.out.println(al.indexOf(20.5f));

		//lastIndexOf(element)
		System.out.println(al.lastIndexOf(20.5f));

		//get(int)
		System.out.println(al.get(3));

		//set(index,)
		System.out.println(al.set(3,"Incubator"));

		System.out.println(al);
	
		al.ensureCapacity(3);
		System.out.println(al);
		System.out.println(al.size());
		
		ArrayList al2 = new ArrayList(2);

		al2.add("Salman");
		al2.add("SRK");
		al2.add("Amir");

		System.out.println(al2);
		System.out.println(al2.size());

		al.addAll(al2);
		al.addAll(3,al2);

		System.out.println(al);

//		al.remove(3);
		al.remove(3);
		System.out.println(al);

		Object[] arr = al.toArray();

			System.out.println("t "+arr);
			System.out.println();
		for(var p : arr){
		
			System.out.println(p);
		}
		//add(int ,al)
		al.add(3,al2);
		System.out.println(al);
		System.out.println("remove"+al.remove(3));
		System.out.println(al);

		//removeRange(int,int)
		al.removeRange(3,5);
		System.out.println(al);

		ArrayList al3 = new ArrayList();
		al3.add(new ITCOMP("XYZ",10));
		System.out.println(al3);

		ListIterator litr = al.listIterator(1);

		while(litr.hasNext()){
		
			System.out.println(litr.next());
		}
	}

}
