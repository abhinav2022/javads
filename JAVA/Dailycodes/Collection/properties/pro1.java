
import java.util.*;
import java.io.*;

class Demo{

	public static void main(String[] ar) throws IOException{
	
		Properties obj = new Properties();
		
		FileInputStream fobj = new FileInputStream("friends.properties");

		obj.load(fobj);

		String name = obj.getProperty("ashish");

		System.out.println(name);

		obj.setProperty("Shashi","Biencaps");

		FileOutputStream obj2 = new FileOutputStream("friends.properties");

		obj.store(obj2,"updated by ak");
	}
}
