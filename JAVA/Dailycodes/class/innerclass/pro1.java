
class Outer{

	int x=9;
	static int y =30;

	class Inner extends Outer{
	
		int x = 10;
		static final int y = 30;
		Inner(){
		
			System.out.println(this);
			System.out.println("In Inner");
		}
		
		class Inner2{
		
			Inner2(){}

			void fun3(){
			
				System.out.println("fun3");
				System.out.println(x);
			}
		}
		void fun2(){
		
			//System.out.println(this.this$0.x);
			System.out.println(x);
			System.out.println(y);
			System.out.println("Fun2-OuterInner");
		}

		void rI(){}
	}

	void fun1(){
	
		System.out.println("Fun1-Outer");
	}
}

class Client{

	public static void main(String[] ar){
	
		Outer obj = new Outer();

		Outer.Inner  ob1 = obj.new Inner();

		ob1.fun2();
	}
}
