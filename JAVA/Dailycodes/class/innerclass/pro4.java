
class Outer{

	Object m1(){
	
		class Inner{
		
			void m2(){
			
				System.out.println("Inner m2");
			}
		}

		return new Inner();
	}
}

class Client{

	public static void main(String[] ar){
	
		Outer obj = new Outer();

		((Inner)(obj.m1())).m2();
	}
}
