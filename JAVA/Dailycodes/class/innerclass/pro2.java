
class Outer{

	int x = 10;
	static int y = 20;

	class Inner{
	
		final static int a =20;
		
		 void fun(){
		
			System.out.println("In fun Inner");
		}
			
	}
}

class Client{

	public static void main(String[] e){
	
		System.out.println(Outer.Inner.a);
		Outer.Inner.fun();
	}
}
