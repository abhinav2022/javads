
interface Demo{

	void m1();
}

class Outer {

	int x =20;
	Demo m1(){
	
		//int x=18;

		class Inner implements Demo{
		
			final static int x =9;
			public void m1(){
			
				System.out.println(x);				
			}
		}

		Inner obj = new Inner();
		obj.m1();
		System.out.println(obj.x);

		return obj;
	}
}

class Client{

	public static void main(String[] ar){
	
		Outer obj = new Outer();
		obj.m1().m1();
	}
}
