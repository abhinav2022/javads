
class Demo1{

	void marry(){
	
		System.out.println("Kriti Sanon");
	}
}

class Demo2{

	void marry(){
	
		System.out.println("Disha patani");
	}
}

interface Hello{

	default void greet(){
	
		System.out.println("Hello");
	}
}

class Client{

	public static void main(String[ ] a){
	
		new Demo1(){
			
			int x = 20;

			void m1(){
			
				System.out.println("m1");
			}
		}.m1();

		new Demo2(){
		
			void m2(){
			
				System.out.println("m2");
			}

		}.m2();

		 Hello ob= new Hello(){
		
			public void greet(){
			
				System.out.println("Namaskar");
			}

			 void m3(){
			
				 System.out.println("m3 ");
			 }
		};

		  new Hello(){
		
			public void greet(){
			
				System.out.println("Namaskar");
			}

			 void m3(){
			
				 System.out.println("m3 ");
			 }

			 public final static void main(){
			 
				 System.out.println("sdf");
			 }
		}.m3();
		//ob.m3();
	}
}
