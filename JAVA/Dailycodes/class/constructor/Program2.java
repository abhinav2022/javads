
class Demo{

	{
	
		System.out.println("Instance block1");
	}
	Demo(){
	
		System.out.println("In Constructor");
	}

	public static void main(String[] ar){
	
		System.out.println("In main");
		Demo obj = new Demo();
	}

	{
	
		System.out.println("Instance block2");
	}
}
