
class Demo{

	int x = 10;

	Demo(){
		this.x=9;
		System.out.println("In no-args constructor");
		System.out.println(this);
	}
	Demo(int x){
	
		this();
		System.out.println("In Para constructor");
		System.out.println(this);
	}
	public static void main(String[] ar){
	
		Demo obj = new Demo(128);
		System.out.println(obj.x);
	}
}
