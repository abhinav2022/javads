
class Demo{

	Demo(){
	
		System.out.println("No args");
		System.out.println(this);
	}

	Demo(Demo o){
	
		System.out.println("with parameter ");
		System.out.println("this "+this);
		System.out.println(o);
	}

	public static void main(String[] ar){
	
		Demo obj = new Demo();
		Demo obj1 = new Demo(obj);
		obj1 = new Demo(obj1);
	}
}
