
class Demo{

	static {
	
		System.out.println("static block1");
	}
	{
		System.out.println("Instance block1");
	}
	Demo(){
	
		System.out.println("In Constructor");
	}

	public static void main(String[] ar){
	
		System.out.println("In main");
		Demo obj = new Demo();
		Pro3 obj1 = new Pro3();
	}

	static {
	
		System.out.println("static block2");
	}
	{
	
		System.out.println("Instance block2");
	}
}

class Pro3{

	static {
	
		System.out.println("In static Pro3");
	}

	{
	
		System.out.println("In Pro3 Constructor");
	}
}
