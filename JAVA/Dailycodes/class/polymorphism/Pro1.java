
class Parent{

	void fun(){
	
		System.out.println("In parent fun");
	}
}

class Child extends Parent{

	Child(){
	
		super();
	//	System.out.println(super);
	}

	void fun(){

		System.out.println("In Child fun");
	}

	public static void main(String[] ar){
	
		Parent obj = new Child();
		obj.fun();
	}
} 
