
class Demo1{

	Demo1(){
	
		System.out.println("In Demo1");
	}

	void fun(){
	
		System.out.println("In fun Demo1");
	}
}

class Demo2 extends Demo1{

	Demo2(){
	
		System.out.println("In Demo2");
	}
	Demo2(Demo2 xyz){
	
		System.out.println(this);
		System.out.println(xyz);
		System.out.println("In Demo2 xyz");
	}
	void fun(){
	
		System.out.println("In fun 2");
	}

	public static void main(String[] ar){
	
		Demo1 obj = new Demo2();
		Demo2 obj1 = new Demo2(new Demo2(null));
		obj.fun();
	}
}

