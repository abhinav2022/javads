
class IndianMilitary{

	IndianMilitary(int p){
		DEFCON = p;
	}

	static int TotalActivePersonnel = 2250000;

	int DEFCON ;
	
	double Defence_Budget= 72.4;

	String COAS ="Manoj Pandey";

	static int Helicopters = 438;

	static int jets = 632;

	String latest_rifle = "ak-203";

	void intelligenceInput(){
	
		System.out.println("Intel about Enemies\n");
	}

	void defensiveAction(){

		if(DEFCON ==5){
			System.out.println("No war Ongoing");

		}else if(DEFCON ==3){
		
			intelligenceInput();
			System.out.println("Defensive measures by Army and AirForce ");
		}else if(DEFCON == 2){
		
			intelligenceInput();
			System.out.println("Defensive measures by Army,AirForce and Navy ");
		}
	}

	public static void main(String[] ar){
	
		IndianMilitary obj = new IndianMilitary(5);	
		System.out.println("Chief Of Army Staff At Present is "+obj.COAS);
		System.out.println("Total Strength of Indian Military is "+TotalActivePersonnel);
		System .out.println("Defence Budget of India is "+obj.Defence_Budget+" Billion Dollars");
		System .out.println("Defence Condition level is "+obj.DEFCON);
		obj.defensiveAction();
	}
}
