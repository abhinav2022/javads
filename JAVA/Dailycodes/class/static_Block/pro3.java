
class Demo{

       private int x = 10;
       private String str= "Private";

	void disp(){
	
		System.out.println(x);
		System.out.println(str);
	}

	public static void main(String[] ar){
	
		System.out.println();
		Pro3 obj = new Pro3();
		obj.main(null);
	}

}

class Pro3{

	void main(String[] ar){
	
		Demo obj = new Demo();
		obj.disp();  //error
	}
}
