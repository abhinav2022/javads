
class Demo{

	static {
	
		System.out.println("Static block");
	}

	public static void main(String[] a){
	
		System.out.println("Main Method");
	}

	static {
		int y = 29;
		System.out.println("Static block 2");
		System.out.println(y);
	}
}
