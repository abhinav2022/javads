
class YieldDemo extends Thread{

	public void run(){
	
		for(int i=0;i<10;i++){
		
			System.out.println("In run");
		

		try{
		
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		
		
		}
		if(i%5==0)
			yield();

		}

	}
}

class Demo{

	public static void main(String[] ar){
	
		YieldDemo obj = new YieldDemo();
		obj.start();

		for(int i=0;i<10;i++){
		
			if(i%5==0)
				obj.yield();

			System.out.println("In main");
		}
	}
}
