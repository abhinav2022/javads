
class Demo extends Thread{

	public  void run(){
	
		if(Thread.currentThread().getName().equals("t1")){
		
			for(int i=0;i<10;i++){
			
				System.out.println("t1");
			}
		}
		if(Thread.currentThread().getName().equals("t2")){
		
			for(int i=0;i<10;i++){
			
				System.out.println("t2");
			}
		}
		
	}

	public static void main(String[] ar){
	
		ThreadGroup tg1= new ThreadGroup("tg1 new");
		ThreadGroup tg2= new ThreadGroup(tg1,"tg2 new");

		Demo obj = new Demo();
		Thread t1 = new Thread(tg1,obj,"t1");
		Thread t2 = new Thread(tg1,obj,"t2");

		t1.start();
		t2.start();
			//Thread.currentThread().yield();	
		for(int i=0;i<=10;i++){
		
			System.out.println("In main");
		}
	}
}
