
class ThreadDemo extends Thread{

	//ThreadDeddmo(){}

	public void run(){
	
		System.out.println(Thread.currentThread().getName()+" Priority "+Thread.currentThread().getPriority());

		for(int i=0;i<19;i++){
		
			System.out.println("In run "+Thread.currentThread().getName());
		}
	}
}

class TDemo{

	public static void main(String[] args){
	
		ThreadDemo obj = new ThreadDemo();
		obj.setPriority(4);
		obj.start();
		ThreadDemo obj1 = new ThreadDemo();
		obj1.start();
		
		System.out.println(Thread.currentThread().getName()+" Priority "+Thread.currentThread().getPriority());

		for(int i=0;i<19;i++){
		
			System.out.println("In main");
		}
	
	}
}
