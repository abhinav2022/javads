
import java.io.*;
import java.util.*;

class Demo{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println(br.readLine());

		br.reset();
		System.out.println(br.readLine());

	}
}
