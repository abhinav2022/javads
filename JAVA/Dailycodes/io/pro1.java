
import java.io.*;

class Demo{

	public static void main(String[] ar) throws IOException{
	
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

		String str1= br1.readLine();
		System.out.println("str1 "+str1);
		br1.close();
		System.out.println(br1);
		String str2 = br2.readLine();
		Demo obj = new Demo();
		System.out.println("str2 "+str2);
		System.out.println(obj);
		obj = null;
		System.out.println(obj);

	}
}
