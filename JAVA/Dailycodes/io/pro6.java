
import java.io.*;
import java.util.*;

class Demo{

	public static void main(String[] ar) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String info = br.readLine();

		StringTokenizer obj = new StringTokenizer(info);

		System.out.println(obj.countTokens());

		while(obj.hasMoreTokens()){
		
			System.out.println(obj.nextToken());
		}
		
		System.out.println(obj.countTokens());
	}
}
