
import java.io.*;
import java.util.*;

class Demo{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter name,wing, room ,income ");
		String info = br.readLine();

		StringTokenizer obj = new StringTokenizer(info," ");
		String name = obj.nextToken();
		char ch = (obj.nextToken()).charAt(0);
		int r = Integer.parseInt(obj.nextToken());
		float income = Float.parseFloat(obj.nextToken());

		System.out.println("Apartment "+name);
		System.out.println("wing "+ch);
		System.out.println("Room number "+r);
		System.out.println("income "+income);
	}
}
