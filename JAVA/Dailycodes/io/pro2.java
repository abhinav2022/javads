
import java.io.*;

class Demo{

	public static void main(String[] ar) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int a = Integer.parseInt(br.readLine());
		char ch = (char)br.read();
		br.skip(1);
		int b = Integer.parseInt(br.readLine());

		System.out.println(a);
		System.out.println(ch);
		System.out.println(b);
	}
}
