
class Demo{

	public static void main(String[] ar){
	
		Runnable obj1 = new Runnable(){
		
			public void run(){
			
				System.out.println(Thread.currentThread());
			}
		};
		Runnable obj2 = new Runnable(){
		
			public void run(){
			
				System.out.println(Thread.currentThread());
			}
		};

		Thread t1 = new Thread(obj1);
		Thread t2 = new Thread(obj2);

		t1.start();
		t2.start();

	}
}
