
import java.io.*;

class FileReaderDemo{

	public static void main(String[] ar) throws IOException{
	
		FileInputStream fis = new FileInputStream("../Incubator");

		FileDescriptor fd = fis.getFD();

		FileReader fr = new FileReader(fd);

		int data =fr.read();

		while(data!=-1){
		
			System.out.print((char)data);
			data=fr.read();
		}
		fr.close();
	}
}
