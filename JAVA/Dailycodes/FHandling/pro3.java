
import java.io.*;

class Demo{

	public static void main(String[] ar) throws IOException{
	
		File fobj = new File("code1");	
		fobj.createNewFile();
		System.out.println(fobj.getParent());
		System.out.println(fobj.getPath());
		System.out.println(fobj.lastModified());
		System.out.println(fobj.getAbsolutePath());

		System.out.println(fobj.canRead());
		System.out.println(fobj.canWrite());
		System.out.println(fobj.isDirectory());
		System.out.println(fobj.isFile());
	}
}
