
import java.io.*;

class Player implements Serializable{

	int jerNo=0;
	String pname;

	Player(int jerNo,String pName){
	
		this.jerNo=jerNo;
		this.pname=pName;
	}
}

class SerializationDemo{

	public static void main(String[] ar)throws IOException{
	
		Player obj1 = new Player(1,"KLRahul");
		Player obj2 = new Player(45,"Rohit Sharma");

		FileOutputStream fos = new FileOutputStream("Player.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(obj1);
		oos.writeObject(obj2);

		oos.close();
		fos.close();
	}
}
