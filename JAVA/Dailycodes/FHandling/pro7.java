
import java.io.*;

class FileReadDemo{

	public static void main(String[]a ) throws IOException{
	
		FileReader fr = new FileReader("../Incubator");

		int data = fr.read();
		
		while(data!=-1){
		
			System.out.print((char)data);
			data=fr.read();
		}
		fr.close();
	}
}
