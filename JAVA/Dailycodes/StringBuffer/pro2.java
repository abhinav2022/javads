
class Demo{

	static StringBuffer reverse(StringBuffer str){

		if(str==null)
		return null;

		char arr[]= str.toString().toCharArray();

		int start =0,end=arr.length-1;

		while(start<end){
		
			char t = arr[start];
			arr[start]=arr[end];
			arr[end]=t;
			start++;
			end--;
		}

		return new StringBuffer(new String(arr));
	} 

	static String reverse(String str){
	
		char arr[]= str.toCharArray();

		int start =0,end=arr.length-1;

		while(start<end){
		
			char t = arr[start];
			arr[start]=arr[end];
			arr[end]=t;
			start++;
			end--;
		}

		return new String(arr);
	} 
/*	static StringBuffer insert(int a,String str,String str1){
	
		if(str==null)
			return null;
		char arr[]= str.toCharArray();

	//	if(a<0 || a>=arr.length)


		char arr1[]= new char[arr.length+a];

	}*/

	public static void main(String[] ar){
	
		String str1 ="C2w";
		String str2 = "Biencaps";

		StringBuffer str3 = new StringBuffer(str1 + str2);

		System.out.println(str3);
		System.out.println(reverse(str3));

		System.out.println(str3.insert(1,1));
	}
}
