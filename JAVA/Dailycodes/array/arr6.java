
import java.io.*;

class Demo{

	public static void f(char p ){
	
		System.out.println(p);
	}

	public static void main(String[] ar,int u)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		byte p = Byte.parseByte(br.readLine());

		f(p);
	}
}
