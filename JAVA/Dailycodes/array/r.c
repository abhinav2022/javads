
#include<stdio.h>
#include<stdlib.h>

void fun(int((*arr)[])){

	printf("%ld\n",sizeof(arr));
}

void main(){

	int *arr= malloc(4*sizeof(int));
	int ptr[4];
	fun(&ptr);
	printf("%ld\n",sizeof(arr));
	printf("%ld\n",sizeof(ptr));

}
