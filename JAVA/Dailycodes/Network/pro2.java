
import java.net.*;
import java.io.*;

class IPAddressDemo{

	public static void main(String[]a )throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = br.readLine();

		InetAddress ip = InetAddress.getByName(s);

		System.out.println("IP address = "+ip);
	}
}
