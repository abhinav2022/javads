
import java.util.concurrent.*;

class MyThread implements Runnable{

	int num;

	MyThread(int num){
	
		this.num=num;
	}

	public void run(){
	
		System.out.println(Thread.currentThread()+" Start "+num);
		task();
		System.out.println(Thread.currentThread()+" End "+num);
		System.out.println();
	}

	void task(){
	
		try{
			System.out.println();
			Thread.sleep(8000);
		}catch(InterruptedException ire){}
	}
}

class ThreadPoolDemo{

	public static void main(String[] ar){
	
		ExecutorService ser =  Executors.newSingleThreadExecutor();

		for(int i=0;i<6;i++){
		
			MyThread obj = new MyThread(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}
