
class MyThread implements Runnable{

	public void run(){
	
		System.out.println(Thread.currentThread());
		try{
		
			Thread.sleep(5000);
		}catch(InterruptedException ie){
		
			System.out.println(ie);
		}
	}
}

class ThreadGroupDemo {

	public static void main(String[] ar){
	
		ThreadGroup pthread = new ThreadGroup("India");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1 = new Thread(obj1,"Maha");
		Thread t2 = new Thread(obj2,"Goa");

		t1.start();
		t2.start();
	}
}
