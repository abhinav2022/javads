
class MyThreadGroup extends Thread {

	MyThreadGroup (ThreadGroup tg,String str){
	
		super(tg,str);
	}

	public void run(){
	
		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo {

	public static void main(String[] ar){
	
		ThreadGroup pthreadgp = new ThreadGroup("Core2Web");

		MyThread obj1 = new MyThread(pthreadgp ,"c");

		MyThread obj2 = new MyThread(pthreadgp,"c++");
		
		obj1.start();
	}
}
