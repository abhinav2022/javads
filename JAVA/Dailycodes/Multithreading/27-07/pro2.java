
class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}

	public void run(){
	
		System.out.println(Thread.currentThread());

		try{
		
			Thread.sleep(300);

		}catch(InterruptedException ie){
		
			System.out.println("Catch");
			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo{

	public static void main(String[] ar) throws InterruptedException{
	
		ThreadGroup pthreadgp = new ThreadGroup("India");

		MyThread t1 = new MyThread(pthreadgp,"Maha");
		MyThread t2 = new MyThread(pthreadgp,"Goa");

		t1.start();
		t2.start();

		ThreadGroup cthreadgp = new ThreadGroup (pthreadgp,"Pak");

		MyThread t3 = new MyThread(cthreadgp,"Karachi");
		MyThread t4 = new MyThread(cthreadgp,"Lahore");

		t3.start();
		t4.start();

		ThreadGroup cthreadgp1 = new ThreadGroup (pthreadgp,"Bangla");
	MyThread t5 = new MyThread(cthreadgp1,"Dhaka");
		MyThread t6 = new MyThread(cthreadgp1,"mirpur");
		t5.start();
		t6.start();
		cthreadgp.interrupt();
	/*	try{
		
			//Thread.sleep(2000);
		}catch(InterruptedException ie){}*/
		System.out.println("active count "+pthreadgp.activeCount());
		System.out.println(pthreadgp.activeGroupCount());

	}

}
