
class ThreadName extends Thread{

	ThreadName( ){
	
		super();
	}
	ThreadName(String str ){
	
		super(str);
	}

	public void run(){
	
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadDemo {

	public static void main(String[] ar){
	
		ThreadName obj1 = new ThreadName("name");
		ThreadName obj2 = new ThreadName("name");
		ThreadName obj3 = new ThreadName();
		ThreadName obj4 = new ThreadName();

		obj1.start();
		obj2.start();
		obj3.start();
		obj4.start();
		//obj1.run();
		//obj2.run();
	}	
}
