
class Mythread extends Thread{

	public void run(){
	
		System.out.println(Thread.currentThread().getName());
		for(int i=0;i<10;i++){
			
			System.out.println(Thread.currentThread().getName());
		}
	}
}

class ThreadYieldDemo{

	public static void main(String[] ar){
	
		Mythread obj = new Mythread();
		obj.start();

		Thread.currentThread().yield();

		for(int i=0;i<10;i++){
			
			System.out.println(Thread.currentThread().getName());
		}
		//System.out.println(Thread.currentThread().getName());
	}
}
