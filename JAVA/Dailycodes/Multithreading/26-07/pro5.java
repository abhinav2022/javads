
class MyThreadG extends Thread{

	MyThreadG(ThreadGroup tg,String str){
	
		super(tg,str);
	}

	public void run(){
	
		System.out.println(Thread.currentThread());
	}
}

class Demo{

	public static void main(String[] ar){
	
		ThreadGroup pthreadgp = new ThreadGroup("Core2Web");

		MyThreadG obj1 = new MyThreadG(pthreadgp,"C");

		MyThreadG obj2 = new MyThreadG(pthreadgp,"Java");

		obj1.start();
		obj2.start();

	}
}
