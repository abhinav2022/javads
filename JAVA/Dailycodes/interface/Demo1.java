
interface Demo{

	static int x=0;
	default void m2(){
	System.out.println("In demo m2");
}
	static void m1(){
	
		System.out.println("In demo");
	}
}

interface Demo1 extends Demo2{

	static int x = 2;
	default void m2(){
	System.out.println("In demo1 m2");
}
	static void m1(){
	
		System.out.println("In Demo1");
	}
}

interface Demo2 extends Demo,Demo1{

}
abstract class Demo3 implements Demo1{

	int x=0;
	static void m1(){
	
		System.out.println("abstract");
	}
}
class DemoChild extends Demo3{

	//void m2(){}
	public static void main(String [] ar){
	
		Demo1 p = new DemoChild();
		Demo.m1();
		Demo1.m1();
System.out.println();
		p.m2();
		System.out.println(p.x);
//		DemoChild 

	}
	 
}
