
class A{

	int x = 10;
	void fun(){
	
		System.out.println("In A");
	}

}

class B extends A{

	int x = 1;
	void fun(){
	
		System.out.println("In B");
	}

	public static void main(String[] ar){
	
		B b = new B();
		((A)b).fun();
		System.out.println(((A)b).x);
	}
}
