
interface Demo{

	static int x=0;

	static void m1(){
	
		System.out.println("In demo");
	}
}

interface Demo1 {

	static int x = 2;

	static void m1(){
	
		System.out.println("In Demo1");
	}
}

interface Demo2 extends Demo,Demo1{


}
/*abstract class Demo1 {

	int x;

	static void m1(){
	
		System.out.println("abstract");
	}
}*/

class DemoChild implements Demo2,Demo{


	public static void main(String [] ar){
	
		Demo1 p = new DemoChild();
		Demo.m1();
		Demo1.m1();

		System.out.println(p.x);
//		DemoChild 

	}
	 
}
