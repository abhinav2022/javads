
interface A{

	default void m1(){
	
		System.out.println("In m1");
	}
}

interface B extends A{

	default void m1(){
	
		System.out.println("In m1 B");
	}
}

class Demo implements A,B{


	public void m1(){
	
		System.out.println("In Demo m1\n");
	}
	public static void main(String[] ar){
	
		Demo obj = new Demo();
		(obj).m1();
	}
}
