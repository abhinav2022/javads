
enum ProgLang{

	c,cpp,java,py
}

class EnumDemo{

	public static void main(String[] a){
	
		System.out.println(ProgLang.c);

		ProgLang lang = ProgLang.java;

		System.out.println(lang);
		System.out.println(lang.name());
		System.out.println(lang.ordinal());
	}
}
