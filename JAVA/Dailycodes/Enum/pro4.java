
class Parent{

	@Deprecated
	void m1(){
	
		System.out.println("Parent m1");
	}
}

class Client{

	@SuppressWarnings("XYZ")
	public static void main(String[] ar){
	
		Parent p = new Parent();
		p.m1();
	}
}
