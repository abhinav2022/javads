
class StringDemo{

	public static void main(String[] ar){
	
		String str1 = "Abhi";
		String str2 = "Kachole";

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		str1.concat(str2);
		System.out.println();

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
	}

}
