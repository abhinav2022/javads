
class StringDemo{

	public static void main(String[] ar){
	
		String str1 = "Abhi";
		String str2 = "Abhi";
		String str3 = str1 + str2;
		String str4 = str1 + str2;
		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
	//	System.out.println(System.identityHashCode(str3));
		//str1.concat(str2);
		System.out.println();
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
		System.out.println(str3.hashCode()==str4.hashCode());
		System.out.println(str1.hashCode()==str2.hashCode());

		//System.out.println(System.identityHashCode(str1));
		//System.out.println(System.identityHashCode(str2));
	}

}
