import java.io.*;
class Capbuf{

	public static void main(String[] ar)throws IOException{
	
		StringBuffer str = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println(str.capacity());
		StringBuffer str1 = new StringBuffer(br.readLine());//"PLJG1");

		System.out.println(str1.capacity());
		System.out.println(str1);
		System.out.println(str1.hashCode());
		System.out.println(System.identityHashCode(str1));

		str1 = str1.append(br.readLine());//"qwerty123456789911111111111111111111");
		System.out.println(str1);
		System.out.println(str1.hashCode());
		System.out.println(System.identityHashCode(str1));
		System.out.println(str1.capacity());
		str1 = str1.append(br.readLine());//"qwerty123456789911111111111111111111");
		System.out.println(str1);
		System.out.println(str1.hashCode());
		System.out.println(System.identityHashCode(str1));
		System.out.println(str1.capacity());
	}
}
