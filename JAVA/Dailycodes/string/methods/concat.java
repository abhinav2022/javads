
class Demo{

	static String myconcat(String str1,String str2){
	
		char arr1[]=str1.toCharArray();
		char arr2[]=str2.toCharArray();

		char arr3[]=new char[arr1.length+arr2.length];

		int itr=0;

		for(;itr<arr1.length+arr2.length;itr++){
		
			if(itr<arr1.length)
				arr3[itr]=arr1[itr];
			else
				arr3[itr]=arr2[itr-arr1.length];
		}

		return new String(arr3);
	}

	public static void main(String[] ar){
	
		String str1 = "C2W";
		String str2 = "Biencaps";

		System.out.println(myconcat(str1,str2));
	}
}
