
import java.io.*;

class Q10{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());

		System.out.println();
		for(int i=a ;i<=b;i++){
		
			int cnt = 0;
			for(int j=1;j<=i;j++){
			
				if(i%j==0)
					cnt++;
			}
			if(cnt<3)
				System.out.print(i+" ");
		}
		System.out.println();
	}
}
