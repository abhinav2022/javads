
import java.io.*;

class Q9{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int a = Integer.parseInt(br.readLine());
		System.out.println();

		int s=0;

		while(a>0){
		
			int y = a%10,x=1;
			while(y>0){
			
				x*=y--;				
			}
			s+=x;
			a/=10;
		}
		System.out.println("Addition is "+s);
	}
}
