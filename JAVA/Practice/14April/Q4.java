
import java.io.*;

class Q4{

	public static void main(String[] ar)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());

		for(int i=b ;i>=a;i--){
		
			if(i%2==0)
				System.out.print(i+" ");
		}
		System.out.println();
		for(int i=a ;i<=b;i++){
		
			if(i%2!=0)
				System.out.print(i+" ");
		}
		System.out.println();
	}
}
